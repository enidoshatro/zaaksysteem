BEGIN;

ALTER TABLE thread_message_attachment ADD COLUMN uuid UUID NOT NULL UNIQUE DEFAULT uuid_generate_v4();

COMMIT;
