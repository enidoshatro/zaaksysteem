BEGIN;

    ALTER TABLE message ADD is_archived BOOLEAN NOT NULL DEFAULT FALSE;
    CREATE INDEX message_subject_archived_idx ON message(subject_id, is_archived);
    CREATE INDEX message_subject_read_idx ON message(subject_id, is_read);

COMMIT;
