BEGIN;

    ALTER TABLE zaaktype_relatie ADD COLUMN subject_role text[];
    ALTER TABLE zaaktype_relatie ADD COLUMN copy_subject_role boolean default false;
COMMIT;

