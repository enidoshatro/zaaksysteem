BEGIN;
    INSERT INTO roles (parent_group_id, name, description, system_role, date_created, date_modified)
    SELECT id, 'Klantcontacter', 'Systeemrol: Klantcontacter', true, now(), now() from groups where path = array[groups.id];
COMMIT;
