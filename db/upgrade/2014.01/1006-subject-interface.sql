BEGIN;

INSERT INTO interface (name, active, max_retries, interface_config, multiple, module) (
    SELECT 'LDAP Authenticatie',true,'10','{}', true, 'authldap' WHERE NOT EXISTS (
        SELECT * FROM interface WHERE module = 'authldap'
    )
);

COMMIT;
