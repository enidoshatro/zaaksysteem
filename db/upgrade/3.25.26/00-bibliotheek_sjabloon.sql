BEGIN;
    ALTER TABLE bibliotheek_sjablonen RENAME template_uuid TO template_external_name;
    ALTER TABLE bibliotheek_sjablonen ALTER template_external_name TYPE text;
COMMIT;

