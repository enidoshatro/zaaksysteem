BEGIN;
    /* db/upgrade/2015.04/1000-object_data_invalid_flag.sql */

    ALTER TABLE object_data ADD COLUMN invalid BOOLEAN DEFAULT FALSE;

    /* db/upgrade/2015.04/1001-better_indexes.sql */
    DROP INDEX IF EXISTS object_data_hstore_to_timestamp_idx;
    DROP INDEX IF EXISTS object_data_hstore_to_timestamp_idx2;

    CREATE INDEX object_data_hstore_to_timestamp_idx ON object_data(hstore_to_timestamp(index_hstore->'case.date_of_completion'));
    CREATE INDEX object_data_hstore_to_timestamp_idx2 ON object_data(CAST(hstore_to_timestamp(index_hstore->'case.date_of_completion') AS DATE));

    /* db/upgrade/2015.04/1002-kenmerk_multiple_label.sql */

    ALTER TABLE zaaktype_kenmerken ADD COLUMN label_multiple TEXT;

    /* db/upgrade/2015.04/1003-transaction_indexes.sql */

    CREATE INDEX transaction_date_created_idx ON transaction ( date_created desc ) WHERE date_deleted is null;
    CREATE INDEX transaction_record_to_object_transaction_record_id_idx ON transaction_record_to_object(transaction_record_id);

COMMIT;
