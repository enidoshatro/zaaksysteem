#!/usr/bin/env bash

set -e

CONTAINER_BUILD_DIR="/opt/zaaksysteem"

cd ${CONTAINER_BUILD_DIR}/client
echo Step 1/4: Installing '/client' dependencies
npm install --no-optional

cd ${CONTAINER_BUILD_DIR}/frontend
echo Step 2/4: Installing '/frontend' dependencies
npm install --no-optional

cd ${CONTAINER_BUILD_DIR}/client
echo Step 3/4: Building '/client'
npm run build-apps

cd ${CONTAINER_BUILD_DIR}/frontend
echo Step 4/4: Building '/frontend'
npm run fullbuild
