/*global angular,fetch,define,XMLHttpRequest,FormData,window*/
(function ( ) {
	
	window.zsDefine('nl.mintlab.docs.FileUpload', function ( ) {
		
		var inherit = window.zsFetch('nl.mintlab.utils.object.inherit'),
			bind = angular.bind,
			EventDispatcher = window.zsFetch('nl.mintlab.events.EventDispatcher'),
			addEventListener = window.zsFetch('nl.mintlab.utils.events.addEventListener');
		
		function FileUpload ( file, name ) {
			this.completed = false;
			this.error = null;
			this.ended = false;
			this.progress = 0;
			this.loadedBytes = NaN;
			this.totalBytes = NaN;
            this.file = file;
            this.name = name || 'file';
		}
		
		inherit(FileUpload, EventDispatcher);
		
		function onProgress ( event ) {
			// chrome fires a progress event with loaded&total=0
			if(event.total) {
				this.loadedBytes = event.loaded;
				this.totalBytes = event.total;
			}
			this.progress = this.loadedBytes/this.totalBytes;
			this.publish('progress', this, this.progress);
		}
		
		function onLoad ( /*event*/ ) {
			this.progress = 1;
			this.publish('progress', this, this.progress);
			this.completed = true;
			this.publish('complete', this, this.xhr.responseText);
			bind(this, end)();
		}
		
		function onError ( event ) {
			this.error = true;
			this.publish('error', this, event);
			bind(this, end)();
		}
		
		function onAbort ( event ) {
			this.error = true;
			this.publish('error', this, event);
			bind(this, end)();
		}
		
		function end ( ) {
			if(!this.ended) {
				this.ended = true;
				this.publish('end', this);
			}
		}
		
		function onReadyStateChange ( event ) {
			var readyState = event.target.readyState,
				status;
				
			if(readyState !== 2) {
				return;
			}
			
			status = event.target.status;
			if(status >= 400) {
				this.error = true;
				this.publish('error', this, event);
				end.call(this);
			}
		}
		
		FileUpload.prototype.send = function ( url, params ) {
			
			var xhr = new XMLHttpRequest(),
				fd = new FormData(),
				key,
				token = window.getXSRFToken();
				
			this.xhr = xhr;
				
			addEventListener(xhr.upload, 'progress', bind(this, onProgress));
			// ff doesn't fire a progress event for 1
			addEventListener(xhr.upload, 'load', bind(this, onProgress));
			addEventListener(xhr.upload, 'error', bind(this, onError));
			addEventListener(xhr, 'load', bind(this, onLoad));
			addEventListener(xhr, 'error', bind(this, onError));
			addEventListener(xhr, 'abort', bind(this, onAbort));
			addEventListener(xhr, 'readystatechange', bind(this, onReadyStateChange));
				
			xhr.open('POST', url);
			
			if(token) {
				xhr.setRequestHeader('XSRF-TOKEN', token);
			}
			
			xhr.withCredentials = true;
			if(params && params.filename){
				fd.append(this.name, this.file, params.filename);
				delete params.filename;
			} else {
				fd.append(this.name, this.file);
			}
			for(key in params) {
				fd.append(key, params[key]);
			}
			xhr.send(fd);
			this.publish('start', this);
		};
		
		FileUpload.prototype.abort = function ( ) {
			this.xhr.abort();
		};
		
		FileUpload.prototype.getData = function ( ) {
			var data = this.xhr.responseText;
			try {
				data = JSON.parse(data);
			} catch( error ) {
				console.log('error', error);
				data = {};
			}
			return data;
		};
		
		FileUpload.prototype.getObjects = function ( ) {
			var data = this.getData();
			if(data) {
				return data.result;
			}
			return [];
		};
		
		return FileUpload;
		
	});
		
})();
