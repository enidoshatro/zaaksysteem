/*global angular*/
(function ( ) {

	angular.module('Zaaksysteem.docs')
		.directive('zsMintloaderFile', [ '$window', '$http', 'systemMessageService',  function ( $window, $http, systemMessageService ) {

			return {
				scope: true,
				controller: [ '$scope', '$element', function ( $scope, $element ) {

					var ctrl = this;

					ctrl.handleDocClick = function ( event ) {
						$window.open($scope.downloadUrl);
						event.stopPropagation();
					};

					ctrl.handleRemoveClick = function ( event ) {

						var fileId = Number($scope.fileId),
							listItem = $element.parent(),
							files,
							fileAccepted = ($element[0].attributes['data-zs-accepted'].value == "true"),
							fileRejected = ($element[0].attributes['data-zs-reject-to-queue'].value == "true"),
							remove_action;
						
						event.stopPropagation();
						event.preventDefault();
						
						listItem.css('display', 'none');

						remove_action = fileRejected ? { rejected: true } : { deleted: true };
						
						files = {};
						files[fileId] = 
							{
								action: 'update_properties',
								data: remove_action
							};

						$http({
							url: '/api/bulk/file/update',
							method: 'POST',
							data: {
								files: files
							}
						})
							.success(function ( ) {
								listItem.remove();
								$scope.$destroy();
							})
							.error(function ( ) {
								listItem.css('display', '');
								
								systemMessageService.emitError('Het bestand kon niet worden verwijderd. Probeer het later opnieuw.');
							});

					};

				}],
				controllerAs: 'mintloaderFile'
			};

		}]);
}());
