import angular from 'angular';
import 'lodash';
import 'jquery';
import 'jquery-migrate';
import 'jquery-ui/ui/core';
import 'jquery-ui/ui/effect';
import 'jquery-ui/ui/position';
import 'jquery-ui/ui/button';
import 'jquery-ui/ui/draggable';
import 'jquery-ui/ui/droppable';
import 'jquery-ui/ui/resizable';
import 'jquery-ui/ui/widget';
import 'jquery-ui/ui/selectmenu';
import 'jquery-ui/ui/datepicker';
import 'jquery-ui/ui/sortable';
import 'jquery-ui/ui/accordion';
import 'jquery-ui/ui/tabs';
import 'jquery-ui/ui/autocomplete';
import 'jquery-ui/ui/dialog';
import resourceModule from '../client/src/shared/api/resource';
import zsResourceConfiguration from '../client/src/shared/api/resource/resourceReducer/zsResourceConfiguration';
import zsInternModule from '../client/src/intern/zsIntern/zsViewlessIntern/index.js';
import zsTruncateModule from '../client/src/shared/ui/zsTruncate';
import zsTooltipModule from '../client/src/shared/ui/zsTooltip';
import zsSnackbarModule from '../client/src/shared/ui/zsSnackbar';
import createCaseRegistrationModule from '../client/src/shared/case/createCaseRegistration';
import createContactModule from '../client/src/shared/contact/createContact';
import createContactMomentModule from '../client/src/intern/views/root/createContactMoment';
import contextualActionServiceModule from '../client/src/shared/ui/zsContextualActionMenu/contextualActionService';
import viewTitleModule from '../client/src/shared/util/route/viewTitle/index.js';
import zsVormTemplateModifierModule from '../client/src/shared/zs/vorm/zsVormTemplateModifier';
import caseStatusLabelFilterModule from '../client/src/shared/case/caseStatusLabelFilter';
import caseStatusIconModule from '../client/src/shared/case/zsCaseStatusIcon';
import zsDisabledModule from '../client/src/shared/ui/zsDisabled';
import zsMoveFocusToModule from '../client/src/shared/ui/zsMoveFocusTo';
import zsTrapKeyboardFocusModule from '../client/src/shared/ui/zsTrapKeyboardFocus';
import composedReducerModule from '../client/src/shared/api/resource/composedReducer';
import sessionServiceModule from '../client/src/shared/user/sessionService';
import zsStorageModule from '../client/src/shared/util/zsStorage';
import assign from 'lodash/object/assign';
import difference from 'lodash/array/difference';
import proj4 from 'proj4';

window.Proj4js = window.Proj4js || proj4;

// Define "Amersfoort" (Rijksdriehoek) projection, used for a lot of Dutch map layers
window.Proj4js.defs('EPSG:28992','+proj=sterea +lat_0=52.15616055555555 +lon_0=5.38763888888889 +k=0.9999079 +x_0=155000 +y_0=463000 +ellps=bessel +towgs84=565.417,50.3319,465.552,-0.398957,0.343988,-1.8774,4.0725 +units=m +no_defs');

window.angular
    .module('zs-new-abstract', [
        composedReducerModule,
        sessionServiceModule
    ]);

window.angular
    .module('zs-new', [
        'zs-new-abstract',
        caseStatusIconModule,
        caseStatusLabelFilterModule,
        contextualActionServiceModule,
        createCaseRegistrationModule,
        createContactModule,
        createContactMomentModule,
        resourceModule,
        viewTitleModule,
        zsInternModule,
        zsSnackbarModule,
        zsStorageModule,
        zsTooltipModule,
        zsTruncateModule,
        zsVormTemplateModifierModule,
        zsDisabledModule,
        zsMoveFocusToModule,
        zsTrapKeyboardFocusModule
    ])
    .constant('HOME', '/')
    .run([
        '$rootScope', 'contextualActionService', 'composedReducer', 'sessionService', 'zsStorage',
        ($rootScope, contextualActionService, composedReducer, sessionService, zsStorage) => {
            let currentLocation = window.location.pathname;

            if (currentLocation.indexOf('/auth/') === 0) {
                zsStorage.clear();
            }

            if (currentLocation.indexOf('/auth/') !== 0 && currentLocation.indexOf('/pip') !== 0 && currentLocation.indexOf('/form') !== 0) {
                let permissions,
                    userResource = sessionService.createResource($rootScope, { cache: { every: 15 * 60 * 1000 } }),
                    getPermissions;

                let includesRoles = (session, roles) => {
                    return difference(roles, session.instance.logged_in_user.system_roles).length < roles.length;
                };

                permissions = composedReducer({ scope: $rootScope }, userResource)
                    .reduce((user) => {

                        return {
                            contact: includesRoles(user, ['Administrator', 'Zaaksysteembeheerder', 'Contactbeheerder']),
                            contactmoment: includesRoles(user, ['Administrator', 'Zaaksysteembeheerder', 'Contactbeheerder']),
                            case: true
                        };
                    });

                getPermissions = permissions.data;

                let actions = [
                    {
                        name: 'zaak',
                        label: 'Zaak aanmaken',
                        iconClass: 'folder-outline',
                        template:
                            `<create-case-registration
                                     on-close="close($promise)"
                                     casetype-id="casetypeId"
                                     data-file="params.file"
                                     data-requestor="params.requestor"
                                     data-file="params.file"
                                 ></create-case-registration>`,
                        params: {
                            casetypeId: null
                        }
                    },
                    {
                        name: 'contact',
                        label: 'Contact aanmaken',
                        iconClass: 'account-plus',
                        template: '<create-contact on-close="close($promise)"></create-contact>',
                        when: ['viewController', () => !!getPermissions().contact]
                    },
                    {
                        name: 'contact-moment',
                        label: 'Contactmoment toevoegen',
                        iconClass: 'comment-plus-outline',
                        template:
                            `<create-contact-moment
                                     on-submit="close($promise)"
                                     data-subject="params.subject"
                                     case-id="params.caseId"
                                 ></create-contact-moment>`
                    }
                ]
                    .map(action => {

                        return assign({
                            click: () => {
                                contextualActionService.openAction(action);
                            }
                        }, action);
                    });

                contextualActionService.getAvailableActions = () => actions;
            }

        }])
    .run([
        '$window', '$rootScope', '$document', 'viewTitle',
        ($window, $rootScope, $document, viewTitle) => {
            let title = {};

            let getLegacyTitle = () => {
                let titleEl = $document[0].querySelector('.block-header-title'),
                    fallbackEl = $document.find('title');

                title.mainTitle = titleEl ? titleEl.innerHTML : fallbackEl[0].innerHTML;

                if (!title) {
                    title.mainTitle = 'Zaaksysteem';
                }

                if ($window.location.href.indexOf('/beheer') !== -1) {
                    title.mainTitle = 'Beheer';
                }

            };

            viewTitle.get = () => title;
            $rootScope.$watch(getLegacyTitle);
        }])
    .config(['resourceProvider', (resourceProvider) => {
        zsResourceConfiguration(resourceProvider.configure);
    }]);

window.angular = angular;
