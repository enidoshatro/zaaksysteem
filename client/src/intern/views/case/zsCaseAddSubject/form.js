import angular from 'angular';

export default ( magicStringResource, roles ) => {

	return {
		fields:
			[
				{
					name: 'relation_type',
					label: 'Relatietype',
					template: 'radio',
					data: {
						options: [
							{
								value: 'natuurlijk_persoon',
								label: 'Burger'
							},
							{
								value: 'bedrijf',
								label: 'Organisatie'
							},
							{
								value: 'medewerker',
								label: 'Medewerker'
							}
						]
					},
					required: true
				},
				{
					name: 'related_subject',
					label: 'Betrokkene',
					template: 'object-suggest',
					data: {
						objectType: [ '$values', ( values ) => values.relation_type ]
					},
					required: true
				},
				{
					name: 'related_subject_role',
					label: 'Rol betrokkene',
					template: 'select',
					data: {
						options:
							roles ?
								roles
									.asMutable()
									.map(role => {
										return {
											value: role.instance.label,
											label: role.instance.label
										};
									})
								: []
					}
				},
				{
					name: 'magic_string_prefix',
					label: 'Magic string prefix',
					template: {
						inherits: 'text',
						display: ( el ) => {

							return angular.element(
								`<div>
									${el[0].outerHTML}
									<zs-spinner is-loading="vm.invokeData('pending')" class="spinner-tiny"></zs-spinner>
								</div>`
							);

						}
					},
					data: {
						pending: ( ) => magicStringResource.state() === 'pending'
					},
					disabled: true,
					required: true
				},
				{
					name: 'pip_authorized',
					label: 'Gemachtigd voor deze zaak',
					template: 'checkbox',
					when: [ '$values', ( vals ) => vals.relation_type !== 'medewerker' ],
				},
				{
					name: 'employee_authorisation',
					label: 'Toegestane rechten op de zaak',
					template: 'select',
					when: [ '$values', ( vals ) => vals.relation_type === 'medewerker' ],
					data: {
						options: [
							{
								value: 'none',
								label: 'Geen rechten'
							},
							{
								value: 'search',
								label: 'Zoeken'
							},
							{
								value: 'read',
								label: 'Raadplegen'
							},
							{
								value: 'write',
								label: 'Behandelen'
							},
						]
					},
				},
				{
					name: 'notify_subject',
					label: 'Verstuur bevestiging per email',
					template: 'checkbox',
					when: [ '$values', ( vals ) => vals.relation_type !== 'medewerker' ],
				}
			]
	};

};
