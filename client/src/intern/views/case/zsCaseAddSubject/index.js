import angular from 'angular';
import angularUiRouterModule from 'angular-ui-router';
import mutationServiceModule from './../../../../shared/api/resource/mutationService';
import actionsModule from './../zsCaseRelationView/actions';
import vormFieldsetModule from './../../../../shared/vorm/vormFieldset';
import vormObjectSuggestModule from './../../../../shared/object/vormObjectSuggest';
import selectModule from './../../../../shared/vorm/types/select';
import inputModule from './../../../../shared/vorm/types/input';
import radioModule from './../../../../shared/vorm/types/radio';
import vormValidatorModule from './../../../../shared/vorm/util/vormValidator';
import zsSpinnerModule from './../../../../shared/ui/zsSpinner';
import resourceModule from './../../../../shared/api/resource';
import composedReducerModule from './../../../../shared/api/resource/composedReducer';
import snackbarServiceModule from './../../../../shared/ui/zsSnackbar/snackbarService';
import seamlessImmutable from 'seamless-immutable';
import get from 'lodash/get';
import formCtrl from './form';
import getRoleValue from './getRoleValue';
import createMagicStringResource from './createMagicStringResource';
import createRoleResource from './createRoleResource';
import template from './template.html';
import messages from './../../../../shared/vorm/util/vormValidator/messages';

export default
	angular.module('zsCaseAddSubject', [
		vormFieldsetModule,
		angularUiRouterModule,
		mutationServiceModule,
		actionsModule,
		vormObjectSuggestModule,
		selectModule,
		inputModule,
		radioModule,
		vormValidatorModule,
		resourceModule,
		composedReducerModule,
		zsSpinnerModule,
		snackbarServiceModule
	])
		.directive('zsCaseAddSubject', [
			'$http', '$state', 'resource', 'composedReducer', 'vormValidator', 'snackbarService', 'mutationService',
			( $http, $state, resource, composedReducer, vormValidator, snackbarService, mutationService ) => {

			return {
				restrict: 'E',
				template,
				scope: {
					caseId: '&',
					onSubjectAdd: '&'
				},
				bindToController: true,
				controller: [ '$scope', function ( scope ) {

					let ctrl = this;

					let values = seamlessImmutable( {
						relation_type: 'natuurlijk_persoon',
						related_subject_role: 'Advocaat',
						employee_authorisation: 'none',
					});

					let magicStringResource = createMagicStringResource(resource, scope, ( ) => ({ role: getRoleValue(values), caseId: ctrl.caseId() }));

					let roleResource = createRoleResource(resource, scope);

					let form = formCtrl(magicStringResource, roleResource.data());

					let fields = form.fields;

					let validityReducer = composedReducer( { scope }, fields, ( ) => values)
						.reduce( ( formFields, vals ) => {

							return vormValidator(formFields, vals, messages);

						});

					magicStringResource.onUpdate( ( ) => {

						values = values.merge({ magic_string_prefix: magicStringResource.data() });

					});

					roleResource.onUpdate( ( ) => {

						form = formCtrl(magicStringResource, roleResource.data());
						fields = form.fields;

					});

					ctrl.values = ( ) => values;

					ctrl.handleChange = ( name, value ) => {

						values = values.merge({ [name]: value });

					};

					ctrl.getFields = ( ) => fields;

					ctrl.isValid = ( ) => get(validityReducer.data(), 'valid');

					ctrl.isDisabled = ( ) => !ctrl.isValid() || magicStringResource.state() !== 'resolved';

					ctrl.getValidity = ( ) => get(validityReducer.data(), 'validations');

					ctrl.handleSubmit = ( ) => {

						let promise =
							mutationService.add({
								type: 'case/relation/subject/add',
								// make sure mutations are picked up by resources
								request: {
									url: `/api/case/${ctrl.caseId()}/subjects`,
									params: {
										zapi_num_rows: 20,
										zapi_page: 1
									}
								},
								data: {
									caseId: ctrl.caseId(),
									betrokkeneType: values.relation_type,
									betrokkene_identifier: `betrokkene-${values.relation_type}-${values.related_subject.data.id}`,
									magic_string_prefix: values.magic_string_prefix,
									role: getRoleValue(values),
									notify_subject: !!values.notify_subject,
									employee_authorisation: values.employee_authorisation,
									pip_authorized: !!values.pip_authorized,
								},
							}).asPromise();

						ctrl.onSubjectAdd({ $promise: promise });

					};

				}],
				controllerAs: 'vm'
			};

		}])
		.name;
