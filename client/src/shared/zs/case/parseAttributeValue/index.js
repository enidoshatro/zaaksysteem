/**
 * Used in:
 * - ZTB phase registration
 */
import get from 'lodash/get';
import map from 'lodash/map';
import identity from 'lodash/identity';
import { stringToNumber } from '../../../util/number';

export default function parseAttributeValues(attribute, source) {
	let value = source;

	if (value === null) {
		return value;
	}

	switch (attribute.type) {
	case 'bag_adres':
	case 'bag_straat_adres':
	case 'bag_openbareruimte':
		return value ? get(value, 'bag_id') : null;
	case 'bag_adressen':
	case 'bag_straat_adressen':
	case 'bag_openbareruimtes':
		return map(value, (val) => {
			return val ? get(val, 'bag_id') : null;
		})
			.filter(identity);
	case 'date':
		value = value || value === 0 ? new Date(value) : null;

		if (!value || isNaN(value.getTime())) {
			return null;
		}

		return `${value.getDate()}-${value.getMonth() + 1}-${value.getFullYear()}`;
	case 'numeric':
		if (!Array.isArray(value)) {
			value = [value];
		}

		return value.map(input => stringToNumber(input));
	case 'valuta':
	case 'valutaex':
	case 'valutaex21':
	case 'valutaex6':
	case 'valutain':
	case 'valutain21':
	case 'valutain6':
		return stringToNumber(value);
	default:
		return value;
	}
}
