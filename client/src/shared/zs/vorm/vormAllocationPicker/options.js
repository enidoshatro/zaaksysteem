export default [
	{
		name: 'me',
		label: 'Zelf in behandeling nemen'
	},
	{
		name: 'coworker',
		label: 'Specifieke behandelaar'
	},
	{
		name: 'org-unit',
		label: 'Rol of afdeling'
	}
];
