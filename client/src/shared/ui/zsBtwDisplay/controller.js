import startsWith from 'lodash/startsWith';
import { toCurrency, toNumber } from '../../util/number';
import multipliers from './multipliers';

export default class BtwDisplayController {
	/**
	 * @param {Function} currencyFilter
	 */
	constructor( currencyFilter ) {
		this.currencyFilter = currencyFilter;
	}

	/**
	 * @return {string}
	 */
	getBtwDescription() {
		return `${startsWith(this.btwType(), 'in') ? 'excl.' : 'incl.'} btw`;
	}

	/**
	 * @param {number} value
	 * @return {number}
	 */
	calculateBtw(value) {
		return value * multipliers[this.btwType()];
	}

	/**
	 * @param {Number} value
	 * @return {string}
	 */
	toCurrency(value) {
		if (isNaN(value)) {
			return '';
		}

		return this.currencyFilter(toCurrency(this.calculateBtw(value)));
	}

	/**
	 * @return {string}
	 */
	getBtwValue() {
		const value = this.value();

		if (value === null) {
			return '';
		}

		return this.toCurrency(toNumber(this.value()));
	}
}
