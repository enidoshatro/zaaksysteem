import htmlsave from 'htmlsave';

export default class TruncateHtmlController {

	static get $inject() {
		return ['$scope', '$sce'];
	}

	constructor( scope, $sce ) {
		const ctrl = this;
		let collapsed = true;
		let html;
		let truncated;

		function setHtml( value ) {
			const val = value ? String(value) : '';
			const truncatedHtml = htmlsave.truncate(val || '', ctrl.length());
			const htmlToDisplay = collapsed ? truncatedHtml : val;

			truncated = truncatedHtml !== val;
			html = $sce.trustAsHtml(htmlToDisplay);
		}

		scope.$watch(() => ctrl.value(), ( value ) => {
			setHtml(value);
		});

		ctrl.getHtml = () => html;

		ctrl.isTruncated = () => truncated;

		ctrl.isCollapsed = () => collapsed;

		ctrl.handleToggle = () => {
			collapsed = !collapsed;
			setHtml(ctrl.value());
		};
	}

}
