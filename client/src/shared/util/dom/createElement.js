const { keys } = Object;

export default function createElement(genericIdentifier, attributes = []) {
  const anchor = document.createElement(genericIdentifier.toUpperCase());

  for (const name of keys(attributes)) {
    anchor.setAttribute(name, attributes[name]);
  }

  return anchor;
}
