import angular from 'angular';
import template from './index.html';
import resourceModule from '../../shared/api/resource';
import caseCompleteViewModule from './caseCompleteView';
import snackbarServiceModule from './../../shared/ui/zsSnackbar/snackbarService';
import auxiliaryRouteModule from './../../shared/util/route/auxiliaryRoute';
import onRouteActivateModule from './../../shared/util/route/onRouteActivate';
import zsModalModule from './../../shared/ui/zsModal';
import viewTitleModule from './../../shared/util/route/viewTitle';
import configServiceModule from '../shared/configService';
import first from 'lodash/first';
import seamlessImmutable from 'seamless-immutable';
import flattenAttrs from '../shared/flattenAttrs';
import './styles.scss';

export default {

	moduleName:
		angular.module('Zaaksysteem.mor.caseComplete', [
			resourceModule,
			caseCompleteViewModule,
			snackbarServiceModule,
			auxiliaryRouteModule,
			onRouteActivateModule,
			zsModalModule,
			viewTitleModule,
			configServiceModule
		])
		.name,
	config: [
		{
			route: {
				url: '/melding/:caseID/afhandelen',
				title: [ ( ) => 'Melding afhandelen' ],
				resolve: {
					caseItem: [ '$rootScope', '$stateParams', '$q', 'resource', 'snackbarService', ( $rootScope, $stateParams, $q, resource, snackbarService ) => {

						let caseResource = resource(
							{ url: '/api/v1/case', params: { zql: `SELECT {} FROM case WHERE case.number = ${$stateParams.caseID}` } },
							{ scope: $rootScope }
						)
							.reduce( ( requestOptions, data ) => {

								return first( (data || seamlessImmutable([])).map(flattenAttrs) );

							});

						return caseResource
							.asPromise()
							.then( ( ) => caseResource)
							.catch( ( err ) => {

								snackbarService.error('De melding kon niet worden geladen. Neem contact op met uw beheerder voor meer informatie.');

								return $q.reject(err);
							});
					}],
					caseType: [ '$rootScope', '$q', 'resource', 'snackbarService', 'caseItem', ( $rootScope, $q, resource, snackbarService, caseItem ) => {

						let caseTypeResource = resource(
							{
								url: `/api/v1/casetype/${caseItem.data().instance.casetype.reference}`,
								params: {
									version: caseItem.data().instance.casetype.instance.version
								}
							},
							{ scope: $rootScope }
						)
							.reduce( ( requestOptions, data ) => {

								return first( (data || seamlessImmutable([])).map(flattenAttrs) );

							});

						return caseTypeResource
							.asPromise()
							.then( ( ) => caseTypeResource)
							.catch( ( err ) => {

								snackbarService.error('Het zaaktype kon niet worden geladen. Neem contact op met uw beheerder voor meer informatie.');

								return $q.reject(err);
							});
					}]
				},
				auxiliary: true,
				onActivate: [ '$timeout', '$rootScope', '$window', '$document', '$compile', 'viewTitle', '$stateParams', 'zsModal', 'caseItem', 'caseType', ( $timeout, $rootScope, $window, $document, $compile, viewTitle, $stateParams, zsModal, caseItem, caseType ) => {

					let scrollEl = angular.element($document[0].querySelector('.body-scroll-container'));

					scrollEl.css({ overflow: 'hidden' });

					let openModal = ( ) => {

						let modal,
							unregister,
							scope = $rootScope.$new(true);

						scope.caseItem = caseItem;
						scope.caseType = caseType;

						modal = zsModal({
							el: $compile(angular.element(template))(scope),
							classes: 'casecomplete-modal'
						});

						modal.open();

						modal.onClose( ( ) => {
							$window.history.back();
							return true;
						});

						unregister = $rootScope.$on('$stateChangeStart', ( ) => {

							$window.requestAnimationFrame( ( ) => {

								$rootScope.$evalAsync(( ) => {
									modal.close()
										.then(( ) => {

											scope.$destroy();

											scrollEl.css({
												overflow: '',
												height: ''
											});

										});

									unregister();

								});

							});
							
						});

					};


					$window.requestAnimationFrame( ( ) => {
						$rootScope.$evalAsync(openModal);
					});

				}]

			},
			state: 'caseComplete'
		}
	]
};
