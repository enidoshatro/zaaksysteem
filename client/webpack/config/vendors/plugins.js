const { resolve } = require('path');
const {
  DllPlugin,
  optimize: {
    UglifyJsPlugin,
  }
} = require('webpack');

module.exports = [
  new DllPlugin({
    context: process.cwd(),
    name: 'zaaksysteem_[name]',
    path: resolve(__dirname, '..', '..', 'manifest', '[name].json'),
  }),
  new UglifyJsPlugin({
    sourceMap: false,
  }),
];
