const { resolve } = require('path');

module.exports = {
  filename: '[name].bundle.js',
  path: resolve(__dirname, '..', '..', 'distribution'),
  library: 'zaaksysteem_[name]',
};
