package TestFor::General::SQL;
use base qw(Test::Class);

use TestSetup;

use Zaaksysteem::SQL;

sub zs_sql : Tests {

    my $x = Zaaksysteem::SQL->new(
        schema  => $zs->schema,
        tmp_dir => $schema->tmp_path,
        version => 'Testsuite',
    );

    my $files;
    my @legit_warnings = (
        "Ignoring relationship 'event' - related resultsource 'Zaaksysteem::Schema::Event' is not registered with this schema",
        "Ignoring relationship 'publish_type' - related resultsource 'Zaaksysteem::Schema::PublishTypes' is not registered with this schema",
        "Ignoring relationship 'checklist' - related resultsource 'Zaaksysteem::Schema::ChecklistAntwoord' is not registered with this schema",
        "Ignoring relationship 'documenten' - related resultsource 'Zaaksysteem::Schema::Documenten' is not registered with this schema",
        "Ignoring relationship 'zaaktype_attributen' - related resultsource 'Zaaksysteem::Schema::ZaaktypeAttributen' is not registered with this schema",
    );

    my @found_warnings;

    {
        local $SIG{__WARN__} = sub { my $w = shift; chomp($w) ; push(@found_warnings, $w) } ;
        $files = $x->sql;
    }

    my @files = keys (%$files);
    is(@files, 1, "One SQL file");
    is($files[-1], "Zaaksysteem-Schema-Testsuite-PostgreSQL.sql");

    is_deeply(\@found_warnings, \@legit_warnings, "Found legit warnings");

}

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2014, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
