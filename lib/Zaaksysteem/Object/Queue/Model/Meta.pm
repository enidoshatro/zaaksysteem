package Zaaksysteem::Object::Queue::Model::Meta;

use Moose::Role;

use BTTW::Tools;
use List::MoreUtils qw[any];

=head1 NAME

Zaaksysteem::Object::Queue::Model::Meta - Meta queue item handlers

=head1 DESCRIPTION

=head1 METHODS

=head2 run_ordered_item_set

=cut

sig run_ordered_item_set => 'Zaaksysteem::Backend::Object::Queue::Component';

sub run_ordered_item_set {
    my $self = shift;
    my $item = shift;

    # Collect all item ids of items already run, so we can fail the
    # right items if an exception occurs.
    my @run_item_ids;

    try {
        for my $item_id (@{ $item->data->{ item_ids } }) {
            my $item = $self->fetch_item($item_id);

            my $sub_item = $self->run_item_externally($item);

            # E-mail sub-items are OK to fail, can be user-error and should
            # not prevent other items from executing.
            if ($sub_item->is_failed && $sub_item->type ne 'send_email') {
                throw('queue/run/failed_item_in_ordered_set', sprintf(
                    'Sub-item failed to run, ordered set run incomplete'
                ));
            }

            push @run_item_ids, $item_id;
        }
    } catch {
        for my $item_id (@{ $item->data->{ item_ids } }) {
            # Skip already executed items
            next if any { $item_id eq $_ } @run_item_ids;

            $self->mark_item($item_id, 'failed');
        }

        $_->throw if (blessed $_ && $_->can('throw'));
        die $_;
    };
}

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2016, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
