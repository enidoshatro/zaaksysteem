package Zaaksysteem::Object::Types::File;

use Moose;

extends 'Zaaksysteem::Object';

=head1 NAME

Zaaksysteem::Object::Types::File - Metadata for file records

=head1 DESCRIPTION

=cut

use BTTW::Tools;

use Zaaksysteem::Types qw[NonEmptyStr];

=head1 ATTRIBUTES

=head2 name

Stores the name of the C<file> object. This will usually be the filename
provided during upload of the file, and should be expected to be
filesystem-safe.

=cut

has name => (
    is => 'rw',
    isa => NonEmptyStr,
    label => 'Name',
    traits => [qw[OA]],
    required => 1
);

=head2 size

Stores the content size in number of bytes.

=cut

has size => (
    is => 'rw',
    isa => 'Int',
    label => 'Size (bytes)',
    traits => [qw[OA]],
    required => 1
);

=head2 mimetype

Stores the interpreted mimetype of the file as a string value.

=cut

has mimetype => (
    is => 'rw',
    isa => 'Str',
    label => 'Mimetype',
    traits => [qw[OA]],
    required => 1
);

=head2 md5

Stores a MD5 content hash of the file.

B<Note> this hash is to be used exclusively for download integrity checks. MD5
is B<not> safe for crytographically strong authenticity validation.

=cut

has md5 => (
    is => 'rw',
    isa => 'Str',
    label => 'Contenthash (MD5)',
    traits => [qw[OA]],
    required => 1
);

=head2 archivable

Stores if the file is considered archivable based on rules of the mimetype

=cut

has archivable => (
    is       => 'rw',
    isa      => 'Bool',
    label    => 'Archivable file',
    traits   => [qw[OA]],
    required => 1
);

=head2 virus_scan_status

Stores the status of the virus scan

=cut

has virus_scan_status => (
    is => 'rw',
    isa => 'Str',
    label => 'Virus scan status',
    traits => [qw[OA]],
    required => 1
);

=head1 METHODS

=head2 TO_STRING

Overrides L<Zaaksysteem::Object/TO_STRING>, returns the L</name> field as
value.

=cut

override TO_STRING => sub { return shift->name };

__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2017, 2018 Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
