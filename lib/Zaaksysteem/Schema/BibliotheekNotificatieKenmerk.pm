use utf8;
package Zaaksysteem::Schema::BibliotheekNotificatieKenmerk;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

=head1 NAME

Zaaksysteem::Schema::BibliotheekNotificatieKenmerk

=cut

use strict;
use warnings;


=head1 BASE CLASS: L<Zaaksysteem::Result>

=cut

use base 'Zaaksysteem::Result';

=head1 TABLE: C<bibliotheek_notificatie_kenmerk>

=cut

__PACKAGE__->table("bibliotheek_notificatie_kenmerk");

=head1 ACCESSORS

=head2 id

  data_type: 'integer'
  is_auto_increment: 1
  is_nullable: 0
  sequence: 'bibliotheek_notificatie_kenmerk_id_seq'

=head2 bibliotheek_notificatie_id

  data_type: 'integer'
  is_foreign_key: 1
  is_nullable: 0

=head2 bibliotheek_kenmerken_id

  data_type: 'integer'
  is_foreign_key: 1
  is_nullable: 0

=cut

__PACKAGE__->add_columns(
  "id",
  {
    data_type         => "integer",
    is_auto_increment => 1,
    is_nullable       => 0,
    sequence          => "bibliotheek_notificatie_kenmerk_id_seq",
  },
  "bibliotheek_notificatie_id",
  { data_type => "integer", is_foreign_key => 1, is_nullable => 0 },
  "bibliotheek_kenmerken_id",
  { data_type => "integer", is_foreign_key => 1, is_nullable => 0 },
);

=head1 PRIMARY KEY

=over 4

=item * L</id>

=back

=cut

__PACKAGE__->set_primary_key("id");

=head1 RELATIONS

=head2 bibliotheek_kenmerken_id

Type: belongs_to

Related object: L<Zaaksysteem::Schema::BibliotheekKenmerken>

=cut

__PACKAGE__->belongs_to(
  "bibliotheek_kenmerken_id",
  "Zaaksysteem::Schema::BibliotheekKenmerken",
  { id => "bibliotheek_kenmerken_id" },
);

=head2 bibliotheek_notificatie_id

Type: belongs_to

Related object: L<Zaaksysteem::Schema::BibliotheekNotificaties>

=cut

__PACKAGE__->belongs_to(
  "bibliotheek_notificatie_id",
  "Zaaksysteem::Schema::BibliotheekNotificaties",
  { id => "bibliotheek_notificatie_id" },
);


# Created by DBIx::Class::Schema::Loader v0.07046 @ 2017-06-15 14:24:43
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:T9CpasgJje4AhQcTvT7guw

__PACKAGE__->load_components(
    "+Zaaksysteem::Helper::ToJSON",
    __PACKAGE__->load_components()
);

# You can replace this text with custom code or comments, and it will be preserved on regeneration
1;



__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=cut

