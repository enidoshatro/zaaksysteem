package Zaaksysteem::Search::ScoredResultSet;

use Moose::Role;
use namespace::autoclean;

use BTTW::Tools;

requires qw[search _build_score_map];

sig _build_score_map => '=> ArrayRef[HashRef[Maybe[Str]|ArrayRef[Str]]]';

=head1 NAME

=head1 REQUIRED METHODS

=head2 _build_score_map

This method must be implemented by consumers of this role.

Every call to L</scored_search> will trigger a deferral to the required method
C<_build_score_map>. The output of which must be an ordered (from highest
scoring to lowest) C<ArrayRef> of C<HashRef>s of column/value pairs.

    [
        {
            'my_column' => $value,
            'other_column' => $other_value
        },
        {
            'second_rank' => $foo
        },
        {
            'third_rank' => [ $bar, $or_baz, $or_quux ]
        }
    ]

The above score map will produce a scoring heuristic where C<my_column> and
C<other_column> are scored with 100 'points' each, if exactly matching.

The C<second_rank> column will receive 10 points if the value matches C<$foo>.

The C<third_rank> column will receive 1 point for every matching value.

The maximum score the above specification can give is 211, assuming the values
for C<third_rank> are different from each other.

=head1 METHODS

=head2 scored_search

This method returns a L<DBIx::Class::ResultSet> configured to function as a
scoring search. A scoring item must be provided, so that the required
C<_build_score_map> method can derive a scoring map.

    my $rs = $scored_rs->scored_search($some_data);

The method also takes an optional search options hash, which will be passed
through to L<DBIx::Class::ResultSet/search_rs>. The keys C<+select>, C<+as>,
and C<order_by> are overridden by this method, if set in the search options.

    my $rs = $scored_rs->scored_search($some_data, {
        offset => 5
    });

=cut

sig scored_search => 'Defined, ?ArrayRef';

sub scored_search {
    my $self = shift;
    my $map = $self->_build_score_map(shift);
    my $search_opts = shift || {};

    my @cases;
    my $iterator = 1;

    for my $column_value (reverse @{ $map }) {
        my @whens;

        # Loop over all columns on current iteration level
        # If a column has defined values, add a SQL fragment and bind param
        # for each one.
        for my $column (keys %{ $column_value }) {
            # Fill @whens with literal sql fragments and bind params
            push @whens, map {
                [
                    sprintf('WHEN %s = ? THEN %d', $column, $iterator),
                    [ {} => $_ ]
                ]
            } grep { defined } map {
                # Automagically unpack simple key value pairs, as well as
                # key => [ $value, ... ] pairs
                ref $_ eq 'ARRAY' ? @{ $_ } : $_
            } ($column_value->{ $column });
        }

        # Produce a case-when sql fragment for the current iteration level
        # Loop over all when fragments, join the literal sql, and lift the
        # bind parameters to the scope of the case fragment sql.
        push @cases, map {
            [
                sprintf(
                    '(CASE %s ELSE 0 END)',
                    join(' ', map { $_->[0] } @whens)
                ),
                map { $_->[1] } $_->[1]
            ]
        } @whens;

        # Move our 'iterator', or scoring scale, to the next step
        # Powers of two would be nicer, but this is easier on the eye for
        # most readers, decrease cognitive load.
        $iterator *= 10;
    }

    my $select = sprintf(
        '(%s) AS score',
        join(' + ', map { $_->[0] } @cases)
    );

    return $self->search_rs(undef, {
        %{ $search_opts },
        '+select' => [ \[ $select, map { $_->[1] } @cases ] ],
        '+as' => [qw[score]],
        order_by => { -desc => 'score' }
    });
}

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2016, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
