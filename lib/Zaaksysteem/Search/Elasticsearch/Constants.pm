package Zaaksysteem::Search::Elasticsearch::Constants;

use warnings;
use strict;
use utf8;

use Exporter qw[import];

=head1 NAME

Zaaksysteem::Search::Elasticsearch::Constants - Contants for dealing with
Elasticsearch data transformations

=cut

our @EXPORT_OK = qw[
    SZG_VALUE_TYPE_MAPPING_MAP
    ZS_VALUE_TYPE_MAPPING_MAP
];

=head1 CONSTANTS

Constants in this package are exported on demand.

=head2 SZG_VALUE_TYPE_MAPPING_MAP

This constant maps L<value type names|Syzygy::Object::ValueType/name> to
an Elasticsearch mapping specification.

These mapping specifications can be used to compose an indexing model for the
Elasticsearch based on L<Syzygy> objects.

=head3 Mapped value types

=over 4

=item complex

Complex values are passed as-is, with the intention that the value is left
alone during indexing. This can be useful for large JSON documents that would
otherwise clog the index.

=item boolean

Boolean values have a direct mapping to ES.

=item datetime

Datetime values are mapped as ES dates, with the strict ISO86001 parser with
optional time component for the value.

=item text

Text values are mapped as-is.

=item string

String values are mapped as the ES C<text> field type.

=item type_name

Type name values are mapped as bare keywords

=item uuid

UUID values are mapped as bare keywords

=item array

Array values are mapped as C<nested> fields. Array items are further
implicitly index as C<object> fields.

=item uri

URI values are broken up as C<object> fields with C<uri>, C<host>, and C<path>
components as sub-fields.

=item object_ref

Object references are broken up as C<object> fields with C<id>, and C<type>
components as sub-fields.

=back

=cut

use constant SZG_VALUE_TYPE_MAPPING_MAP => {
	complex    => { type => 'object' },
	boolean    => { type => 'boolean' },
	datetime   => { type => 'date', format => 'strict_date_optional_time' },
	text       => { type => 'text' },
	string     => { type => 'text' },
	type_name  => { type => 'keyword' },
	uuid       => { type => 'keyword' },
	array      => { type => 'nested' },

	uri        => {
		type       => 'object',
		properties => {
			uri  => { type => 'keyword' },
			host => { type => 'text' },
			path => { type => 'text' }
		}
	},

	object_ref => {
		type       => 'object',
		properties => {
			id   => { type => 'keyword' },
			type => { type => 'keyword' }
		}
	}
};

=head2 SZG_VALUE_TYPE_MAPPING_MAP

This constant maps L<value type names|Syzygy::Object::ValueType/name> to
an Elasticsearch mapping specification.

These mapping specifications can be used to compose an indexing model for the
Elasticsearch based on L<Syzygy> objects.

=head3 Mapped value types

=over 4

=item text

Mapped as a bare C<text> field.

=item textarea

Mapped as a bare C<text> field.

=item bankaccount

Mapped as a bare C<text> field.

=item text_uc

Mapped as a bare C<text> field.

=item file

Mapped as a bare C<text> field.

=item googlemaps

Mapped as a bare C<text> field.

=item email

Mapped as a C<text> field and as a C<keyword> sub-field.

=item url

Mapped as a C<text> field and as a C<keyword> sub-field.

=item option

Mapped as a C<text> field and as a C<keyword> sub-field.

=item select

Mapped as a C<text> field and as a C<keyword> sub-field.

=item checkbox

Mapped as a C<text> field and as a C<keyword> sub-field.

=item geolatlon

Contains the latitude/longitude values as a composite string.

Mapped as a C<geo_point> field.

=item bag_adres

Contains a BAG identifier to an address.

Mapped as a C<keyword> field.

=item bag_adressen

Contains 1+ BAG identifiers to adresses.

Mapped as a C<keyword> field.

=item bag_straat_adres

Contains a BAG identifier to an address.

Mapped as a C<keyword> field.

=item bag_openbareruimte

Contains a BAG identifier to a street.

Mapped as a C<keyword> field.

=item bag_openbareruimtes

Contains 1+ BAG identifiers to streets.

Mapped as a C<keyword> field.

=item bag_straat_adressen

Contains 1+ BAG identifiers to addresses.

Mapped as a C<keyword> field.

=item numeric

Mapped as a bare C<float> field.

=item valuta

Mapped as a C<float> field with a scaling factor of 100, so currency is
indexed with 1/100th precision (accurate up to cents).

=item valutain

Mapped as a C<float> field with a scaling factor of 100, so currency is
indexed with 1/100th precision (accurate up to cents).

=item valutain6

Mapped as a C<float> field with a scaling factor of 100, so currency is
indexed with 1/100th precision (accurate up to cents).

=item valutain21

Mapped as a C<float> field with a scaling factor of 100, so currency is
indexed with 1/100th precision (accurate up to cents).

=item valutaex

Mapped as a C<float> field with a scaling factor of 100, so currency is
indexed with 1/100th precision (accurate up to cents).

=item valutaex6

Mapped as a C<float> field with a scaling factor of 100, so currency is
indexed with 1/100th precision (accurate up to cents).

=item valutaex21

Mapped as a C<float> field with a scaling factor of 100, so currency is
indexed with 1/100th precision (accurate up to cents).

=item date

Mapped as a C<date> field using the C<dd-MM-yyyy> format.

=back

=cut

use constant ZS_VALUE_TYPE_MAPPING_MAP => {
	text                => { type => 'text' },
	textarea            => { type => 'text' },
	bankaccount         => { type => 'text' },
	text_uc             => { type => 'text' },
	file                => { type => 'text' }, # Should have a raw field, but content is filename ffs
	googlemaps          => { type => 'text' }, # Google's text address string
	email               => { type => 'text', fields => { raw => { type => 'keyword' } } },
	url                 => { type => 'text', fields => { raw => { type => 'keyword' } } },
	option              => { type => 'text', fields => { raw => { type => 'keyword' } } },
	select              => { type => 'text', fields => { raw => { type => 'keyword' } } },
	checkbox            => { type => 'text', fields => { raw => { type => 'keyword' } } },
	geolatlon           => { type => 'geo_point' },
	bag_adres           => { type => 'keyword' },
	bag_adressen        => { type => 'keyword' },
	bag_straat_adres    => { type => 'keyword' },
	bag_openbareruimte  => { type => 'keyword' },
	bag_openbareruimtes => { type => 'keyword' },
	bag_straat_adressen => { type => 'keyword' },
	numeric             => { type => 'float' },
	valuta              => { type => 'scaled_float', scaling_factor => 100 },
	valutain            => { type => 'scaled_float', scaling_factor => 100 },
	valutain6           => { type => 'scaled_float', scaling_factor => 100 },
	valutain21          => { type => 'scaled_float', scaling_factor => 100 },
	valutaex            => { type => 'scaled_float', scaling_factor => 100 },
	valutaex6           => { type => 'scaled_float', scaling_factor => 100 },
	valutaex21          => { type => 'scaled_float', scaling_factor => 100 },
	date                => { type => 'date', format => 'dd-MM-yyyy' }
};

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2018, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
