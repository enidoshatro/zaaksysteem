=head1 NAME

Zaaksysteem::Manual::API::V1::Types::Config::Item - Type definition for
config/item objects

=head1 DESCRIPTION

This page documents the serialization of C<config/item> objects.

=head1 JSON

=begin javascript

=end javascript

=head1 INSTANCE ATTRIBUTES

=head2

=head1 COPYRIGHT and LICENSE

Copyright (c) 2016, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
