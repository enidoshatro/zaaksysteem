package Zaaksysteem::StUF::0310::Processor;
use Moose;

with qw/
    MooseX::Log::Log4perl
    Zaaksysteem::StUF::0310::Common
/;

use BTTW::Tools;
use BTTW::Tools::File qw(fix_file_extension_for_fh);
use Crypt::OpenSSL::Random qw(random_pseudo_bytes);
use DateTime;
use File::Temp;
use List::MoreUtils qw(none);
use List::Util qw(first);
use MIME::Base64;
use Zaaksysteem::Backend::Tools::FilestoreMetadata qw(get_document_categories);
use Zaaksysteem::BR::Subject;
use Zaaksysteem::BR::Subject::Constants ':remote_search_module_names';
use Zaaksysteem::Types qw(BSN SequenceNumber);
use Zaaksysteem::XML::Compile;

=head1 NAME

Zaaksysteem::StUF::0310::Processor - Implementations of STUF-ZKN SOAP calls

=head1 DESCRIPTION

StUF-ZKN is a specification for an API to create and manage cases and documents
related to cases.

This class is made to be used from a "process_row" method in one of the
L<Zaaksysteem::Backend::Sysin::Interfaces>.

=head1 ATTRIBUTES

=head2 xml_backend

The L<Zaaksysteem::XML::Compile> instance to use. Defaults to a new
C<Zaaksysteem::XML::Compile> instance, with the StUF-0310 bits loaded, which
should be fine for most use cases.

=cut

has xml_backend => (
    is => 'ro',
    lazy => 1,
    default => sub {
        my $xml_backend = Zaaksysteem::XML::Compile->xml_compile;
        $xml_backend->add_class('Zaaksysteem::XML::Generator::StUF0310');
        return $xml_backend;
    },
);

=head2 schema

L<DBIx::Class> handle to use for accessing the database.

=cut

has schema => (
    is       => 'ro',
    required => 1,
);

=head2 record

The L<Zaaksysteem::Schema::TransactionRecord> instance for the current SOAP action.

=cut

has record => (
    is       => 'ro',
    required => 1,
);

=head2 object_model

The L<Zaaksysteem::Object::Model> instance to use for state advancement.

=cut

has object_model => (
    is       => 'ro',
    required => 1,
);

=head2 betrokkene_model

The L<Zaaksysteem::Betrokkene> instance to use to handle "related people".

=cut

has betrokkene_model => (
    is       => 'ro',
    required => 1,
);

=head2 interface

The interface for which this processor is working

=cut

has interface => (
    is => 'ro',
    required => 1,
);

=head2 municipality_code

The municipality code this processor is using

=cut

has municipality_code => (
    is       => 'ro',
    isa      => 'Str',
    required => 1,
);

=head1 METHODS

=head2 generate_case_id

Generates a new case id, and returns it in the appropriate "Du02" XML wrapper.

This implements the "genereerZaakidentificatie" SOAP call.

=cut

sig generate_case_id => 'XML::LibXML::XPathContext => Str';

sub generate_case_id {
    my ($self, $xpc) = @_;

    my $generated_id = '0000' . $self->schema->resultset('Zaak')->generate_case_id;
    $self->record->transaction_id->update({ external_transaction_id => "case-" . $generated_id });

    my %remote = $self->parse_stuurgegevens($xpc, $xpc->findnodes('ZKN:stuurgegevens'));
    $self->_log_case_id($generated_id, \%remote);

    my $instance = $self->xml_backend->stuf0310;
    my $xml = $instance->generate_case_id_return(
        'writer',
        {
            stuurgegevens => {
                berichtcode => "Du02",
                functie     => "genereerZaakidentificatie",
                $self->build_stuurgegevens($xpc),
            },
            zaak => {
                identificatie => { _ => $generated_id, },
                entiteittype  => 'ZAK',
            },
        },
    );

    return $xml;
}

=head2 _log_case_id

Log the case id generation

=cut

sub _log_case_id {
    my $self = shift;
    my ($generated_id, $remote) = @_;

    my $logging = $self->schema->resultset('Logging');

    $logging->trigger(
        'case/id_requested',
        {
            component    => 'zaak',
            component_id => $generated_id,
            data => {
                remote_system => $remote->{sender},
                interface     => 'STUF-ZKN',
            },
        }
    );

    return;
}

=head2 get_case_document

Retrieve a case document, as specified by the case id and document id.

Expects a C<edcLv01> message as input, and returns an C<edcLa01> message
containing the file information and contents.

=cut

sig get_case_document => 'XML::LibXML::XPathContext => Str';

sub get_case_document {
    my ($self, $xpc) = @_;

    my $file_id = $xpc->findvalue('ZKN:gelijk/ZKN:identificatie');
    my $case_id = $xpc->findvalue('ZKN:gelijk/ZKN:isRelevantVoor/ZKN:gerelateerde/ZKN:identificatie');

    if (!$case_id) {
        # Alternative location of zaak-id (specified in the StUF ZKN spec/standard)
        $case_id = $xpc->findvalue('ZKN:scope/ZKN:object/ZKN:isRelevantVoor/ZKN:gerelateerde/ZKN:identificatie');
    }

    my $file = $self->_get_case_document($file_id, $case_id);

    my %rv = (
        standalone => 1,
        stuurgegevens => {
            berichtcode => "La01",
            entiteittype => "EDC",
            $self->build_stuurgegevens($xpc),
        },
        parameters => {
            indicatorVervolgvraag => 'false',
        },
        antwoord => {
            object => $self->_format_document_full($file),
        },
    );

    my $instance = $self->xml_backend->stuf0310;
    my $xml = $instance->document_response(
        'writer',
        \%rv,
    );

    return $xml;
}

=head2 get_case_document_lock

Retrieve a case document, as specified by a document id.

Expects a C<geefZaakdocumentbewerken_Di02> message as input, and returns an
C<geefZaakdocumentbewerken_Du02> message containing the file information,
locking info and contents.

=cut

sig get_case_document_lock => 'XML::LibXML::XPathContext => Str';

sub get_case_document_lock {
    my ($self, $xpc) = @_;

    my $file_id = $xpc->findvalue('ZKN:edcLv01/ZKN:gelijk/ZKN:identificatie');
    my $file = $self->_get_case_document($file_id);

    my %rv = (
        stuurgegevens => {
            berichtcode => "Du02",
            functie => "geefZaakdocumentbewerken",
            $self->build_stuurgegevens($xpc),
        },
        parameters => {
            checked_out_id => unpack('H*', random_pseudo_bytes(16)),
        },
        antwoord => {
            object => $self->_format_document_full($file),
        },
    );

    my $instance = $self->xml_backend->stuf0310;
    my $xml = $instance->document_response_lock(
        'writer',
        \%rv,
    );

    return $xml;
}

sub _format_document_full {
    my $self = shift;
    my ($file) = @_;

    my $rv = $self->format_case_document_minimal($file);

    my $content = try {
        return $file->filestore_id->content;
    }
    catch {
        die $_;
    };

    $rv->{inhoud} = {
        contentType  => $file->filestore_id->mimetype,
        bestandsnaam => $file->filename,
        _            => $content,
    };

    return $rv;
}

=head2 get_case_details

Retrieves case details as a C<zakLa01> message, given a C<zakLv01> message on
input.

This contains all of the "base" fields in a case (no attributes, strangely), a
list of documents, the case type, a list of the status history.

=cut

sig get_case_details => 'XML::LibXML::XPathContext => Str';

sub get_case_details {
    my ($self, $xpc) = @_;

    my $case;
    if (my @nodes = $xpc->findnodes('ZKN:gelijk')) {
        $case = $self->_get_case($xpc, @nodes);
    }
    else {
        my $begin = $xpc->findvalue('ZKN:vanaf/ZKN:identificatie');
        my $end   = $xpc->findvalue('ZKN:totEnMet/ZKN:identificatie');
        if ($begin ne $end) {
            throw('stuf/zkn/get_case_details/multiple_cases',
                "Zaaksysteem does not support multiple cases in get_case_details request"
            );
        }
        $case = $self->_find_case($xpc, $xpc->findnodes('ZKN:vanaf'));
    }

    my $case_object;

    if($case) {
        $self->_log_case_retrieval($case->id, $xpc);
        $case_object = $self->format_case($case);
    }
    else {
        $self->log->info("Sending empty ZakLa01: ZS-16070");
    }

    return $self->xml_backend->stuf0310->case_response(
        'writer',
        {
            stuurgegevens => {
                berichtcode  => "La01",
                entiteittype => "ZAK",
                $self->build_stuurgegevens($xpc),
            },
            antwoord   => { object                => $case_object },
            parameters => { indicatorVervolgvraag => 'false' },
        }
    );
}

sub _log_case_retrieval {
    my $self = shift;
    my ($case_id, $xpc) = @_;

    my %remote = $self->parse_stuurgegevens($xpc, $xpc->findnodes('ZKN:stuurgegevens'));

    my $logging = $self->schema->resultset('Logging');

    $logging->trigger(
        'case/view',
        {
            component    => 'zaak',
            zaak_id      => $case_id,
            data => {
                remote_system => $remote{sender},
                interface     => 'STUF-ZKN',
            },
        }
    );

    return;
}

sub _synchronise_betrokkenen {
    my $self = shift;
    my ($case, $type, $current_betrokkenen, $betrokkene_nodes) = @_;

    my @new_betrokkenen = map {
        $self->_parse_related($_)
    } @$betrokkene_nodes;

    # For each current_betrokkenen, check if they're in betrokkene_nodes
    for my $b (@$current_betrokkenen) {
        if (!first { $b->betrokkene_identifier eq $_ } @new_betrokkenen) {
            $self->log->trace("Removing ZaakBetrokkene " . $b->id);
            $b->delete();
        }
        else {
            $self->log->trace("ZaakBetrokkene " . $b->id . " does not need to be deleted.");
        }
    }

    # For each betrokkene_nodes, check if they're in current_betrokkenen
    for my $nb (@new_betrokkenen) {
        if ( !first { $nb eq $_->betrokkene_identifier } @$current_betrokkenen ) {
            $self->log->trace("Adding ZaakBetrokkene '$type' - '$nb'");
            $self->_add_case_relation($case, $type, $nb);
        }
        else {
            $self->log->trace("ZaakBetrokkene '$type' - '$nb' already exists");
        }
    }

    return;
}

=head2 generate_document_id

Generates a new document id, and return it in a "Du02" wrapper.

This implements the "genereerZaakidentificatie" SOAP call.

=cut

sig generate_document_id => 'XML::LibXML::XPathContext => Str';

sub generate_document_id {
    my ($self, $xpc) = @_;

    my $generated_id = '0000' . $self->schema->resultset('File')->generate_file_id;

    $self->record->transaction_id->update({ external_transaction_id => "doc-" . $generated_id });

    my $instance = $self->xml_backend->stuf0310;
    my $xml = $instance->generate_document_id_return(
        'writer',
        {
            stuurgegevens => {
                berichtcode => "Du02",
                functie => "genereerDocumentidentificatie",
                $self->build_stuurgegevens($xpc),
            },
            document => {
                identificatie => {
                    _ => $generated_id,
                },
                entiteittype => 'EDC',
            },
        },
    );

    return $xml;
}

=head2 write_case

Create or update a case, based on the received C<zakLk01> message.

If the message contains a "mutatiesoort" "T", it's an addition ("Toevoeging"),
if it has "W" it's a modification ("Wijzigen").

After processing, a "Bv03" return message is generated using the
"stuurgegevens" from the incoming message as a base.

=cut

sig write_case => 'XML::LibXML::XPathContext => Str';

sub write_case {
    my ($self, $xpc) = @_;

    my $mutatie = $xpc->findvalue('ZKN:parameters/StUF:mutatiesoort');
    my $interface = $self->interface;

    if ($mutatie eq 'T') {
        if (not $interface->jpath('$.allow_new_cases')) {
            throw(
                'stufzkn/case_creation_not_allowed',
                'The administrator has disabled case creation using StUF-ZKN',
            );
        }
        $self->_create_case($xpc);
    }
    elsif ($mutatie eq 'W') {
        $self->_update_case($xpc, $interface);
    }
    else {
        throw(
            'stufzkn/unknown_mutatiesoort',
            "Unknown 'mutatiesoort': '$mutatie'"
        );
    }

    my %remote = $self->parse_stuurgegevens($xpc, $xpc->findnodes('ZKN:stuurgegevens'));
    return $self->generate_bv03($self->record->get_column('id'), \%remote);
}

=head2 unlock_case_document

Unlock a case document retrieved for editing by another application.

This is a no-op in Zaaksysteem: we don't support locking/unlocking. It always succeeds.

=cut

sub unlock_case_document {
    my ($self, $xpc) = @_;

    my $instance = $self->xml_backend->stuf0310;
    my $xml = $instance->bv02(
        'writer',
        {},
    );

    return $xml;
}

sub _update_case {
    my ($self, $xpc, $interface) = @_;

    # If the new object has a "heeft" block, the other side is trying to update the status.
    if ($xpc->findnodes('ZKN:object[2]/ZKN:heeft')) {
        if (not $interface->jpath('$.allow_phase_update')) {
            throw(
                'stufzkn/phase_update_not_allowed',
                'The administrator has disabled case status updates using StUF-ZKN',
            );
        }
        return $self->_actualiseer_zaak_status($xpc);
    }
    return $self->_update_zaak($xpc);
}

sub _find_case {
    my $self = shift;
    return try {
        return $self->get_case(@_);
    }
    catch {
        if (blessed($_) && $_->type eq 'stufzkn/case_id/not_integer') {
            die $_;
        }
        return;
    };
}

sub _get_case {
    my ($self, $xpc, $object) = @_;

    my $case_id = $xpc->findvalue('ZKN:identificatie', $object);

    if (!$case_id) {
        throw(
            'stufzkn/case_id/not_found',
            "Unable to find case_id in XML",
            $object,
        );
    }

    $self->log->trace("Found case ID: '$case_id'");

    throw(
        'stufzkn/case_id/not_integer',
        "Case ID '$case_id' is not an integer",
        $object,
    ) unless SequenceNumber->check($case_id);

    my $case = $self->schema->resultset('Zaak')->find($case_id);
    if (!$case || $case->is_deleted) {
        throw(
            'stufzkn/case/not_found',
            "Case with identifier $case_id was not found",
        );
    }

    $self->assert_casetype($case->get_column('zaaktype_id'));
    return $case;
}

sub _actualiseer_zaak_status {
    my ($self, $xpc) = @_;

    # We don't care about the "old" object.
    my ($object) = ($xpc->findnodes('ZKN:object'))[-1];

    my $case = $self->_get_case($xpc, $object);

    my $new_phase = $xpc->findvalue('ZKN:heeft/ZKN:gerelateerde/ZKN:volgnummer', $object);
    if (!defined $new_phase) {
        throw(
            'stufzkn/actualiseer_zaak_status/no_volgnummer',
            "'volgnummer' is mandatory for updating the case status",
        );
    }

    if ($new_phase == 1 && $new_phase == $case->milestone) {
        # StUF-ZKN has a concept of a "case without status", i.e. "just a
        # completed form". Zaaksysteem does not.
        #
        # This lets callers think they set the status, without actually doing
        # anything.
        return;
    }

    if ($new_phase != ($case->milestone + 1)) {
        throw(
            'stufzkn/invalid_status_update',
            sprintf(
                "Current status is '%d', cannot set next status '%d'",
                $case->milestone,
                $new_phase,
            ),
        );
    }

    my $username = $xpc->findvalue('ZKN:heeft/ZKN:isGezetDoor/ZKN:medewerker/ZKN:identificatie', $object);
    # XML::Compile doesn't handle "gerelateerde" in StUF very well. So we default to admin.
    my $medewerker = $self->_find_medewerker_by_username($username || 'admin');

    # TODO Set schema->current_user, so deelzaken can be created
    $case->advance(
        object_model     => $self->object_model,
        betrokkene_model => $self->betrokkene_model,
        current_user     => $medewerker,
    );

    return;
}

sub _set_case_allocation {
    my ($self, $case, $xpc, $object) = @_;

    my $behandelaar = $self->_parse_related($xpc,
        $xpc->findnodes('ZKN:heeftAlsUitvoerende', $object), 1);

    my $coordinator = $self->_parse_related($xpc,
        $xpc->findnodes('ZKN:heeftAlsVerantwoordelijke', $object), 1);

    return if (!$behandelaar && !$coordinator);

    $behandelaar = $behandelaar ? $behandelaar->as_object : undef;
    $coordinator = $coordinator ? $coordinator->as_object : undef;

    throw(
        "stufzkn/uitvoerende_type",
        sprintf(
            'heeftAlsUitvoerende entity identifier not supported (got "%s")',
            $behandelaar->old_subject_identifier)
    ) if $behandelaar && $behandelaar->subject_type ne 'employee';

    throw(
        'stufzkn/verantwoordelijke_type',
        sprintf(
            'heeftAlsVerantwoordelijke entity identifier not supported (got "%s")',
            $coordinator->old_subject_identifier)
    ) if $coordinator && $coordinator->subject_type ne 'employee';

    if ($coordinator) {
        $case->set_coordinator($coordinator->old_subject_identifier);
    }

    if ($behandelaar) {
        $case->set_behandelaar($behandelaar->old_subject_identifier);
        $case->open_zaak($behandelaar);
    };
}

sub _update_zaak {
    my ($self, $xpc) = @_;

    # We don't care about the "old" object.
    my ($object) = ($xpc->findnodes('ZKN:object'))[-1];

    my $case = $self->_get_case($xpc, $object);

    if (my $einddatum = $xpc->findvalue('ZKN:einddatum', $object)) {
        $case->afhandeldatum($self->_parse_stuf_date($einddatum));
    }
    if (my $einddatumGepland = $xpc->findvalue('ZKN:einddatumGepland', $object)) {
        $case->streefafhandeldatum($self->_parse_stuf_date($einddatumGepland));
    }
    if (my $uiterlijkeEinddatum = $xpc->findvalue('ZKN:uiterlijkeEinddatum', $object)) {
        $case->streefafhandeldatum($self->_parse_stuf_date($uiterlijkeEinddatum));
    }
    if (my $registratiedatum = $xpc->findvalue('ZKN:registratiedatum', $object)) {
        # We ignore ZKN:startdatum, because we don't have the 2 separate dates in ZS.
        $case->registratiedatum($self->_parse_stuf_date($registratiedatum));
    }
    if (my $registratiedatum = $xpc->findvalue('ZKN:registratiedatum', $object)) {
        # We ignore ZKN:startdatum, because we don't have the 2 separate dates in ZS.
        $case->registratiedatum($self->_parse_stuf_date($registratiedatum));
    }
    if (my $datumVernietigingDossier = $xpc->findvalue('ZKN:datumVernietigingDossier', $object)) {
        # If result is also set, actual vernietigingsdatum will be calculated.
        # If it's not set, this one stands.
        $case->vernietigingsdatum($self->_parse_stuf_date($datumVernietigingDossier));
    }
    if (my $archiefnominatie = $xpc->findvalue('ZKN:archiefnominatie', $object)) {
        # We ignore ZKN:startdatum, because we don't have the 2 separate dates in ZS.
        $case->archival_state(
            $self->_parse_stuf_boolean($archiefnominatie)
                ? 'overdragen'
                : 'vernietigen'
        );
    }
    # zaakniveau, deelzakenindicatie are implied in ZS, based on whether the
    # case *has* any deelzaken/moederzaak. They can't be set using STUF-ZKN.

    my $result_naam = $xpc->findvalue('ZKN:resultaat/ZKN:omschrijving', $object);
    $case->set_resultaat($result_naam) if $result_naam;

    if(my ($ini) = $xpc->findnodes('ZKN:heeftAlsInitiator', $object)) {
        my $betrokkene_id = $self->_parse_related($xpc, $ini);
        $case->set_aanvrager($betrokkene_id);
    }

    $self->_set_case_allocation(
        $case, $xpc, $object
    );

    $self->_synchronise_betrokkenen(
        $case,
        'belanghebbende',
        [ $case->get_zaak_betrokkenen({ rol => 'Belanghebbende' })->all ],
        [ $xpc->findnodes('ZKN:heeftAlsBelanghebbende', $object) ],
    );

    $self->_synchronise_betrokkenen(
        $case,
        'gemachtigde',
        [ $case->get_zaak_betrokkenen({ rol => 'Gemachtigde' })->all ],
        [ $xpc->findnodes('ZKN:heeftAlsGemachtigde', $object) ],
    );

    $self->_synchronise_betrokkenen(
        $case,
        'overig',
        [
            $case->get_zaak_betrokkenen({
                -and => [
                    { rol => { -not_in => ['Gemachtigde', 'Belanghebbende'] } },
                    { rol => { '!=' => undef } },
                ],
            })->all
        ],
        [ $xpc->findnodes('ZKN:heeftAlsOverigBetrokkene', $object) ],
    );

    $case->update();
    $case->touch();

    return;
}

sub _allowed_casetype_ids {
    my $self = shift;

    my $interface = $self->interface;
    my @allowed_ids = try {
        return $interface->jpath_all('$.casetypes[*].casetype.object_id');
    }
    catch {
        $self->log->info("Unable to query casetypes: $_");
        return ();
    };

    return @allowed_ids;
}

=head2 assert_casetype

Throw if the specified casetype_id doesn't match the casetype configured in the
interface configuration.

=cut

sub assert_casetype {
    my ($self, $casetype_id) = @_;

    my @allowed_ids = $self->_allowed_casetype_ids();

    if (none { $casetype_id == $_ } @allowed_ids) {
        my $casetype = $self->schema->resultset('Zaaktype')->search(
            { 'me.id' => $casetype_id },
            { prefetch => 'zaaktype_node_id' },
        )->first;
        my @casetypes_allowed = $self->schema->resultset('Zaaktype')->search(
            { 'me.id' => \@allowed_ids },
            { prefetch => 'zaaktype_node_id' },
        )->all;
        throw(
            'stufzkn/casetype_mismatch',
            sprintf(
                "Cannot perform action on a case of casetype %s (%d). " .
                "Configured to only allow %s (%s)",
                $casetype ? $casetype->title : '(no name found)',
                $casetype_id // -1,
                @casetypes_allowed ? join(", ", map { $_->title } @casetypes_allowed) : '(no name found)',
                join(", ", @allowed_ids) // '(none)',
            ),
        );
    }

    return;
}

sub _get_requestor {
    my ($self, $xpc, $object) = @_;

    my $requestor = $self->_parse_related(
        $xpc,
        $xpc->findnodes('ZKN:heeftAlsInitiator', $object)
    );
    return $requestor if $requestor;

    throw('stuf/zkn/missing_requestor',
        "Missing requestor in StUF zaken message: ZKN:heeftAlsInitiator missing"
    );
}

sub _create_case {
    my ($self, $xpc) = @_;

    my @objects = $xpc->findnodes('ZKN:object');
    if (@objects > 1) {
        throw(
            'stufzkn/multiple_objects_in_create',
            "Multiple objects found in 'creeerZaak', this is not supported."
        );
    }
    my $object = $objects[0];

    my $identifier = $xpc->findvalue('ZKN:identificatie', $object);
    $self->assert_generated_identifier("case-$identifier");

    # XXX This default is here because XML::Compile 1.40+patch can't handle
    # writing isVan in zakLk01 properly (so our own simulator sends
    # bad/incomplete XML our way).
    my $casetype_code = $xpc->findvalue('ZKN:isVan/ZKN:gerelateerde/ZKN:code', $object) || '1234';
    my $casetype_node = $self->_find_casetype_node_by_code($casetype_code);

    $self->assert_casetype($casetype_node->get_column('zaaktype_id'));

    my $requestor = $self->_get_requestor($xpc, $object);

    my $reg_date = $self->_parse_stuf_date($xpc->findvalue('ZKN:registratiedatum', $object));

    my %create_args = (
        override_zaak_id => 1,
        id               => $identifier,
        zaaktype_node_id => $casetype_node->get_column('id'),
        aanvraag_trigger => 'extern',
        contactkanaal    => 'balie',
        registratiedatum => $reg_date,
        kenmerken => [],
        aanvragers => [
            {
                betrokkene  => $requestor,
                verificatie => 'stufzkn',
            }
        ],
    );

    if ($xpc->findnodes('ZKN:kenmerk', $object)) {
        my $interface = $self->interface;
        my $kenmerk_bron  = $xpc->findvalue('ZKN:kenmerk/ZKN:bron', $object);
        my $kenmerk_value = $xpc->findvalue('ZKN:kenmerk/ZKN:kenmerk', $object);

        # Hardcoded magic string: ztc_extern_kenmerk. Part of the base catalog.
        # If the kenmerk isn't found in this case type, the kenmerk is not saved.
        my $kenmerk = $casetype_node->zaaktype_kenmerken->search_by_magic_strings('ztc_extern_kenmerk')->search(
            { required_permissions => [ undef, '{}' ] },
        )->first;
        if ($kenmerk) {
            push @{ $create_args{kenmerken} }, {
                $kenmerk->get_column('bibliotheek_kenmerken_id') => "$kenmerk_bron|$kenmerk_value"
            };
        }

        $interface->create_related(
            'object_subscription_interface_ids',
            {
                external_id  => sprintf("%s|%s", $kenmerk_bron, $kenmerk_value),
                local_table  => 'zaak',
                local_id     => $identifier,
            }
        );

    }

    if (my $vd = $xpc->findvalue('ZKN:datumVernietigingDossier', $object)) {
        $create_args{vernietigingsdatum} = $self->_parse_stuf_date($vd);
    }
    if (my $an = $xpc->findvalue('ZKN:archiefnominatie', $object)) {
        $create_args{archival_state} = $self->_parse_stuf_boolean($an)
            ? 'overdragen'
            : 'vernietigen';
    }

    try {
        $self->schema->txn_do(sub {

            # TODO: This should go through zaken::model
            # so we don't need to maintain two seperate code paths
            my $case = $self->schema->resultset('Zaak')
                ->create_zaak(\%create_args);

            # Touch a case before we try to allocate it. If we want to
            # allocate it and the case isn't in object data we cannot
            # search on it. Well we can, but no results equals no case
            # or no rights to search on the case. In that case (that was
            # a pun?), no write access and therefore no allocation rights.
            $case->_touch();

            $self->_set_case_allocation($case, $xpc, $object);

            for my $betrokkene (
                $xpc->findnodes('ZKN:heeftAlsGemachtigde', $object))
            {
                my $betrokkene_id
                    = $self->_parse_related($xpc, $betrokkene);
                $self->_add_case_relation($case, 'gemachtigde',
                    $betrokkene_id);
            }
            for my $betrokkene (
                $xpc->findnodes('ZKN:heeftAlsBelanghebbende', $object))
            {
                my $betrokkene_id
                    = $self->_parse_related($xpc, $betrokkene);
                $self->_add_case_relation($case, 'belanghebbende',
                    $betrokkene_id);
            }
            for my $betrokkene (
                $xpc->findnodes('ZKN:heeftAlsOverigBetrokkene', $object))
            {
                my $betrokkene_id
                    = $self->_parse_related($xpc, $betrokkene);
                $self->_add_case_relation($case, 'overig',
                    $betrokkene_id);
            }

            $case->_touch();
        });
    }
    catch {
        $self->log->info("Error creating StUF-ZKN case: $_");
        die $_;
    };

    return;
}

sub _add_case_relation {
    my $self = shift;
    my ($case, $relation_type, $betrokkene_id) = @_;

    $case->betrokkene_relateren(
        {
            betrokkene_identifier => $betrokkene_id,
            rol                   => ucfirst($relation_type),
            magic_string_prefix   => $relation_type,
        }
    );

    return;
}

sub _find_medewerker_by_username {
    my $self = shift;
    my ($username) = @_;

    my $mdw = $self->schema->resultset('Subject')->search(
        {
            subject_type => 'employee',
            username     => lc($username),
        },
    )->first;

    if (!$mdw) {
        throw(
            'stufzkn/medewerker_not_found',
            "No employee found with username '$username'",
        );
    }

    return $mdw;
}

sub _find_bsn {
    my ($self, $bsn) = @_;

    my $np;
    try {
        $np = $self->schema->resultset('NatuurlijkPersoon')->get_by_bsn($bsn, "Yes I want to resurrect");
    }
    catch {
        # We don't want to spam the world
        $self->log->trace($_);
    };
    return $np;
}

sub _import_from_gbav {
    my ($self, $bsn, $interface) = @_;

    my $subject_bridge = Zaaksysteem::BR::Subject->new(
        schema              => $self->schema,
        remote_search       => REMOTE_SEARCH_MODULE_NAME_STUFNP,
        config_interface_id => $interface->id,
    );

    my $np;
    try {
        ($np) = $subject_bridge->search(
            {
                subject_type              => 'person',
                'subject.personal_number' => $bsn,
            }
        );
    }
    catch {
        $self->log->info($_);
    };

    if (!$np) {
        throw(
            "stufzkn/subject/person/notfound/gbav",
            "Unable to find person with BSN $bsn via GBA-V"
        );
    }

    $subject_bridge->remote_import($np);
    return 1;
}

sub _get_natuurlijk_persoon {
    my ($self, $bsn) = @_;

    if (!BSN->check($bsn)) {
        throw('stufzkn/bsn_invalid', "BSN '$bsn' is not elfproef");
    }

    my $np = $self->_find_bsn($bsn);
    return $np if $np;

    my $interface;
    try {
        $interface = $self->_assert_stuf_interface();
    }
    catch {
        $self->log->info($_);
        throw(
            "stufzkn/subject/person/notfound",
            "Unable to find person with BSN $bsn"
        );
    };
    $self->_import_from_gbav($bsn, $interface);
    return $self->_find_bsn($bsn);

}

sub _assert_stuf_interface {
    my $self = shift;

    my $stufconfig = $self->schema->resultset('Interface')->search_active({module => 'stufconfig'});

    my $config;
    while(my $sc = $stufconfig->next) {
        $config = $sc->get_interface_config;

        next if int($config->{gemeentecode}) != int($self->municipality_code);

        if ($config->{gbav_search} || $config->{local_search}) {
            return $sc;
        }
    }
    throw("stufzkn/stufconfig/missing", "Unable to find StUF config interface");
}


sub _parse_related {
    # $no_deflation makes sure that this function returns objects
    # instead of old_subject_identifiers. In other ZS code we use
    # objects more and more and this would otherwise lead to multiple DB
    # calls.
    my ($self, $xpc, $relation, $no_deflation) = @_;
    return if not defined $relation;

    my ($gerelateerde) = $xpc->findnodes('ZKN:gerelateerde', $relation);

    return unless $gerelateerde;

    my $ident;
    if ($ident = $xpc->findvalue('ZKN:medewerker/ZKN:identificatie', $gerelateerde)) {
        my $mdw = $self->_find_medewerker_by_username($ident);
        return $mdw if $no_deflation;
        return sprintf("betrokkene-medewerker-%d", $mdw->id);
    }
    elsif ($ident = $xpc->findvalue('ZKN:organisatorischeEenheid/ZKN:identificatie', $gerelateerde)) {
        throw(
            'stufzkn/unsupported_gerelateerde',
            'Unsupported gerelateerde type "organisatorische eenheid"',
        );
    }
    elsif ($ident = $xpc->findvalue('ZKN:natuurlijkPersoon/BG:inp.bsn', $gerelateerde)) {
        my $np = $self->_get_natuurlijk_persoon($ident);
        return $np if $no_deflation;
        return sprintf("betrokkene-natuurlijk_persoon-%d", $np->id);
    }
    elsif ($ident = $xpc->findvalue('ZKN:nietNatuurlijkPersoon/BG:inn.nnpId', $gerelateerde)) {
        my $company = $self->get_company(rsin => $ident);
        return $company if $no_deflation;
        return sprintf("betrokkene-bedrijf-%d", $company->id);
    }
    elsif ($ident = $xpc->findvalue('ZKN:vestiging/BG:vestigingsNummer', $gerelateerde)) {
        my $company = $self->get_company(location_number => $ident);
        return $company if $no_deflation;
        return sprintf("betrokkene-bedrijf-%d", $company->id);
    }
    elsif ($ident = $xpc->findvalue('StUF:extraElementen/StUF:extraElement[translate(@naam, "KVNUMER", "kvnumer") = "kvknummer"]', $relation)) {
        my $company = $self->get_company(company_number => $ident);
        return $company if $no_deflation;
        return sprintf("betrokkene-bedrijf-%d", $company->id);
    }

    throw(
        'stufzkn/unsupported_gerelateerde',
        "Unsupported gerelateerde type\n" . $gerelateerde->toString(1)
    );
}

sub get_company {
    my $self = shift;
    my %opts = @_;

    # first we will always try to import from the KvK and thus update any exisitng
    try {
        $self->_import_from_kvkapi(%opts);
    }
    catch {
        $self->log->warn("stufzkn/niet_natuurlijk_persoon/import_error: '$_'");
    };

    my %mapping = (
        location_number => 'vestigingsnummer',
        company_number  => 'dossiernummer',
        rsin => 'rsin',
    );

    my ($type) = keys %opts;

    my $company = $self->schema->resultset('Bedrijf')->search({
        $mapping{$type} => $opts{$type}
    })->first;
    return $company if $company;

    throw(
        "stufzkn/niet_natuurlijk_persoon_not_found",
        sprintf(
            "No bedrijf found with '%s' as '%s'", $opts{$type}, $mapping{$type}
        )
    );
}

sub _find_casetype_node_by_code {
    my $self = shift;
    my ($casetype_code) = @_;

    my @casetype_nodes = $self->schema->resultset('ZaaktypeNode')->search(
        {
            code => $casetype_code,
            deleted => undef,
        }
    )->all;
    throw(
        'stufzkn/no_nodes',
        "No casetype found with code '$casetype_code'"
    ) unless @casetype_nodes;
    throw(
        'stufzkn/too_many_nodes',
        "More than one case type found with code '$casetype_code'"
    ) if @casetype_nodes > 1;

    return $casetype_nodes[0];
}

=head2 write_case_document_lock

Process an incoming C<edcLk02> message, which adds a new document to a case.

Returns a C<Bv03> message to indicate success.

=cut

sig write_case_document_lock => 'XML::LibXML::XPathContext => Str';

sub write_case_document_lock {
    my ($self, $xpc) = @_;

    my @objects = $xpc->findnodes('ZKN:edcLk02/ZKN:object');
    my $object = $objects[-1];

    my $document_id  = $self->_get_document_id_from_object($xpc, $object);

    my @casetype_ids = $self->_allowed_casetype_ids();

    my $existing_file = $self->_get_case_document($document_id);

    my $tmp = $self->_write_zkn_inhoud_to_file($xpc, $object);

    my $filename = $self->_find_filename($xpc, $object);
    $filename = fix_file_extension_for_fh($tmp, $filename);

    my $last_version = $existing_file->get_last_version;

    my $admin = $self->_get_admin_subject;
    my $admin_obj = $admin->as_object;

    my $new_file = $last_version->update_file({
        subject       => $admin_obj,
        new_file_path => $tmp->filename,
        original_name => $filename,
        name          => $filename,
        is_restore    => 0,
    });

    my $instance = $self->xml_backend->stuf0310;
    my $xml = $instance->bv02(
        'writer',
        {},
    );

    return $xml;
}

=head2 write_case_document

Process an incoming C<edcLk01> message, which adds a new document to a case.

Returns a C<Bv03> message to indicate success.

=cut

sig write_case_document => 'XML::LibXML::XPathContext => Str';

sub write_case_document {
    my ($self, $xpc) = @_;

    my @objects = $xpc->findnodes('ZKN:object');
    my $object = $objects[-1];

    my $document_id = $self->_get_document_id_from_object($xpc, $object);

    my ($case_node) = $xpc->findnodes('ZKN:isRelevantVoor/ZKN:gerelateerde', $object);
    my $filename = $self->_find_filename($xpc, $object);

    my $case = $self->_get_case($xpc, $case_node);
    my $case_id = $case->id;

    my $existing_file = $self->schema->resultset('File')->search(
        {
            id           => $document_id,
            root_file_id => undef,
            case_id      => $case_id,
        },
    )->first;

    if ($existing_file) {
        $self->log->trace(sprintf(
            "Found existing file with id = '%d' - adding new version", 
            $document_id,
        ));
    } else {
        # New file
        $self->assert_generated_identifier("doc-$document_id");

        $self->log->trace(sprintf(
            "Found newly generated identifier '%d' - adding new file",
            $document_id,
        ));
    }

    my $tmp = $self->_write_zkn_inhoud_to_file($xpc, $object);
    $filename = fix_file_extension_for_fh($tmp, $filename);

    my $file = try {
        my $admin = $self->schema->resultset('Subject')->search(
            {
                username => 'admin',
                subject_type => 'employee',
            }
        )->first;
        my $admin_obj = $admin->as_object;

        if ($existing_file) {
            $self->log->trace("StUF-ZKN - Accepting file with same name as new version of existing file.");
            my $last_version = $existing_file->get_last_version;

            return $last_version->update_file({
                subject       => $admin_obj,
                new_file_path => $tmp->filename,
                original_name => $filename,
                name          => $filename,
                is_restore    => 0,
            });
        }
        else {
            $self->log->trace("StUF-ZKN - Accepting as new file.");
            my %create_args = (
                name       => $filename,
                file_path  => $tmp->filename,
                db_params => {
                    case_id    => $case_id,
                    created_by => $admin_obj->old_subject_identifier,
                    id         => $document_id,
                },
            );

            my $file = $self->schema->resultset('File')->file_create( \%create_args );

            $file->update_properties(
                {
                    subject  => $admin_obj,
                    accepted => 1,
                }
            );
            return $file;
        }
    }
    catch {
        $self->log->fatal($_);
        die $_;
    } ;

    $self->_update_metadata($xpc, $object, $file);

    my %remote = $self->parse_stuurgegevens($xpc, $xpc->findnodes('ZKN:stuurgegevens'));
    return $self->generate_bv03($self->record->get_column('id'), \%remote);
}


sub _update_metadata {
    my ($self, $xpc, $object, $file) = @_;

    my %metadata = (
        description => $xpc->findvalue('ZKN:beschrijving', $object),
    );

    # If there is no vertrouwnlijkheidsaanduiding, default to the DB:
    # Zaakvertrouwelijk
    if (my $tl = $xpc->findvalue('ZKN:vertrouwelijkAanduiding', $object)) {
        # StUF uses ALL CAPS, we only capitalize the first letter
        $metadata{trust_level} = ucfirst(lc($tl));
    }

    if (my $documenttype = $xpc->findvalue('ZKN:dct.omschrijving', $object)) {
        my @categories = get_document_categories;
        my $tmp = lc($documenttype);
        if (my $found = first {$tmp eq lc($_) } @categories) {
            $metadata{document_category} = $found;
        }
        else {
            $self->log->info("Unable to resolve '$documenttype' to a Zaaksysteem document category");
        }
    }

    if (my $vd = $xpc->findvalue('ZKN:creatiedatum', $object)) {
        $metadata{creation_date} = $self->_parse_stuf_date($vd)->strftime('%Y-%m-%d'),
    }

    # Ignored: taal, auteur, titel
    if (my $vd = $xpc->findvalue('ZKN:verzenddatum', $object)) {
        $metadata{origin} = 'Uitgaand';
        $metadata{origin_date} = $self->_parse_stuf_date($vd)->strftime('%Y-%m-%d'),
    }
    elsif (my $od = $xpc->findvalue('ZKN:ontvangstdatum', $object)) {
        $metadata{origin} = 'Inkomend';
        $metadata{origin_date} = $self->_parse_stuf_date($od)->strftime('%Y-%m-%d'),
    }
    $file->update_metadata(\%metadata);
    return 1;
}

=head2 assert_generated_identifier

Assert that the given identifier was generated in an earlier transaction.

Throws C<stufzkn/unclaimed_id> if the identifier can't be found.

=cut

sig assert_generated_identifier  => 'Str';

sub assert_generated_identifier {
    my $self = shift;
    my ($id) = @_;

    my $transaction = $self->schema->resultset('Transaction')->search(
        {
            external_transaction_id => $id,

            interface_id  => $self->record->transaction_id->get_column('interface_id'),
            success_count => { '>' => 0 },
        }
    )->first;

    if(!$transaction) {
        throw(
            'stufzkn/unclaimed_id',
            sprintf("ID '%s' was not generated by an earlier call", $id),
        );
    }

    return;
}

=head2 get_xpath_and_soap_action

Get the L<XML::LibXML::XPathContext> object based on the XML provided.
Also return the soap-call from the XML

    my ($xpath, @actions) = $self->get_xpath_and_soap_action($xml);

=cut

sig get_xpath_and_soap_action => 'Str';

sub get_xpath_and_soap_action {
    my ($self, $xml) = @_;
    my $xc = XML::LibXML::XPathContext->new(XML::LibXML->load_xml(string => $xml));
    $xc->registerNs('SOAP',  'http://schemas.xmlsoap.org/soap/envelope/');
    $xc->registerNs('StUF',  'http://www.egem.nl/StUF/StUF0301');
    $xc->registerNs('ZKN',   'http://www.egem.nl/StUF/sector/zkn/0310');
    $xc->registerNs('BG',    'http://www.egem.nl/StUF/sector/bg/0310');
    $xc->registerNs('xlink', 'http://www.w3.org/1999/xlink');

    # Find the SOAP call by extracting the root element's name from the body
    # in {namespace}nodeName format
    my @nodes = $xc->findnodes('//SOAP:Body/*[1]');
    my @node_names = map {
        '{' . $_->namespaceURI . '}' . $_->localname
    } @nodes;

    # We don't care about the SOAP bits, so use the SOAP body's content as the
    # XPath context node.
    $xc->setContextNode($nodes[0]);
    return ( $xc, @node_names );
}

=head2 build_stuurgegevens

Wrapper around L<Zaaksysteem::StUF::0310::Common> build_stuurgegevens.

=cut

around build_stuurgegevens => sub {
    my $orig = shift;
    my $self = shift;

    return $self->$orig(@_, $self->record->id, "ZKN:stuurgegevens");
};

sub _write_zkn_inhoud_to_file {
    my ($self, $xpc, $object) = @_;

    my $tmp = File::Temp->new();
    print $tmp decode_base64($xpc->findvalue('ZKN:inhoud', $object));
    $tmp->close();

    return $tmp;
}

sub _find_filename {
    my ($self, $xpc, $object) = @_;

    my $filename = $xpc->findvalue('ZKN:titel', $object);
    return $filename if length($filename);

    $filename = $xpc->findvalue('ZKN:inhoud/@StUF:bestandsnaam', $object);
    return $filename if length($filename);

    return 'Undefined filename';
}

sub _parse_stuf_date {
    my $self = shift;
    my ($date) = @_;

    return unless length($date);

    $date =~ /
        ^
        (?<year>[0-9]{4})
        (?<month>[0-9]{2})
        (?<day>[0-9]{2})
    /x or throw(
        'stufzkn/malformed_date',
        "StUF date isn't in the correct format (yyyymmdd expected)"
    );

    return DateTime->new(%+);
}

sub _parse_stuf_boolean {
    my $self = shift;
    my ($value) = @_;

    if ($value eq 'J') {
        return 1;
    }
    elsif ($value eq 'N') {
        return 0;
    }
    else {
        throw(
            'stufzkn/unknown_boolean',
            "Unknown value for boolean: '$value'"
        );
    }
}

sub _get_admin_subject {
    my $self = shift;

    my $admin = $self->schema->resultset('Subject')->search(
        {
            username => 'admin',
            subject_type => 'employee',
        }
    )->first;

    return $admin;
}

sub _import_from_kvkapi {
    my ($self, %opts) = @_;

    my $interface = $self->_assert_kvkapi_interface();

    my $subject_bridge = Zaaksysteem::BR::Subject->new(
        schema              => $self->schema,
        remote_search       => REMOTE_SEARCH_MODULE_NAME_KVKAPI,
        config_interface_id => $interface->id,
    );

    my %mapping = (
        location_number => 'subject.coc_location_number',
        company_number  => 'subject.coc_number',
        rsin            => 'subject.rsin',
    );

    my ($type) = keys %opts;

    my $company;
    try {
        my @companies = $subject_bridge->search(
            {
                subject_type    => 'company',
                $mapping{$type} => $opts{$type}
            }
        );
        if (@companies == 1) {
            $company = $subject_bridge->remote_import($companies[0])
        }
        else {
            die "Could not find unqiue company\n";
        }
    }
    catch {
        throw(
            'stufzkn/subject/company/import_from_kvk/search_error',
            "Unable to search for $mapping{$type} => $opts{$type}: $_"
        );
    };
    return $company;
}

sub _assert_kvkapi_interface {
    my $self = shift;

    my $interface = $self->schema->resultset('Interface')
        ->search_active({ module => 'kvkapi' })->first;

    return $interface if $interface;

    $self->log->info("No active KvK API found");
    throw('stufzkn/kvkapi/missing',
        'Unable to find KvK-API config interface');
}

sub _get_document_id_from_object {
    my ($self, $xpc, $object) = @_;
    my $document_id = $xpc->findvalue('ZKN:identificatie', $object);
    return $document_id if $document_id && int($document_id) > 0;

    throw(
        'stufzkn/document_id/missing',
        'Document ID is missing from XML (ZKN:identificatie)'
    );
}

sig _get_case_document => 'Int,?Int';

sub _get_case_document {
    my ($self, $file_id, $case_id) = @_;

    my @casetype_ids = $self->_allowed_casetype_ids();

    my $file = $self->schema->resultset('File')->active()->search(
        {
            'case_id.deleted'     => undef,
            'case_id.zaaktype_id' => { '-in' => \@casetype_ids },
            'me.case_id'          => ($case_id ? $case_id : { '!=' => undef }),
            'me.active_version'   => 1,
            -and                  => [
                -or => [
                    { 'me.root_file_id' => $file_id },
                    { 'me.id'           => $file_id },
                ],
            ],
        },
        { 'join' => 'case_id', prefetch => 'filestore_id' },
    )->first;

    return $file if $file;

    throw(
        "stufzkn/get_case_document/document_not_found",
        sprintf(
            "No document found with id '%s', attached to case '%d' (of casetype '%s')",
            $file_id, $case_id // '(no case id supplied)', join(", ", @casetype_ids))
    );
}

__PACKAGE__->meta->make_immutable();

=head1 COPYRIGHT and LICENSE

Copyright (c) 2015-2016, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
