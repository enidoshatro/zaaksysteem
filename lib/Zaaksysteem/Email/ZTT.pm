package Zaaksysteem::Email::ZTT;
use Moose;

use BTTW::Tools;
use Email::Address;


=head1 NAME

Zaaksysteem::Email::ZTT - Create emails powered by ZTT

=head1 DESCRIPTION

Generate all kinds of emails with the power of ZTT

=head1 SYNOPSIS

    use Zaaksysteem::Email::ZTT;

    my $thing = Zaaksysteem::Email::ZTT->new(
        case => $case,
    );

    my $segments = $thing->generate_email(
        body => "This is my magic string: [[ magic_string ]]",
        subject => "This is another [[ other_string ]]",,
    );

    # segments now yields
    # {
    #   body => "This is my magic string: some value",
    #   subject => "This is another thing",
    # }

=cut

has case => (
    is        => 'ro',
    isa       => 'Zaaksysteem::Schema::Zaak',
    predicate => 'has_case'
);

has ztt => (
    is      => 'ro',
    isa     => 'Zaaksysteem::ZTT',
    lazy    => 1,
    builder => '_build_ztt',
);

has ztt_context => (
    is        => 'ro',
    isa       => 'Defined',
    predicate => 'has_ztt_context',
);

has betrokkene_model => (
    is        => 'ro',
    isa       => 'Defined',
    predicate => 'has_betrokkene_model',
);

sub _build_ztt {
    my ($self) = @_;

    my $ztt = Zaaksysteem::ZTT->new;
    $ztt->add_context($self->case) if $self->has_case;
    $ztt->add_context($self->ztt_context) if $self->has_ztt_context;
    return $ztt;
}

=head2 generate_email

Generate an e-mail for a case. The case object is required on instantiation of
this model.

Requires C<body> and C<subject> to be have values.

    $self->generate_email(body => 'fo', subject => 'ba');

=cut

define_profile generate_email => (
    required => {
        body    => 'Str',
        subject => 'Str',
    },
    optional => {
        to   => 'Str',
        from => 'Str',
        cc   => 'Str',
        bcc  => 'Str',

        # These influence the 'TO'
        recipient_type => 'Str',
        recipient_role => 'Str',
        recipient      => 'Str',
    },
);

sub generate_email {
    my $self = shift;
    my $opts = assert_profile({@_})->valid;

    throw("case/missing", "Unable to generate preview without a case")
        unless $self->has_case;

    my $mail = $self->generate_preview(%$opts);

    foreach (qw(to subject body)) {
        next if defined $mail->{$_};
        throw("generate_email/missing/$_",
            "Unable to generate e-mail, no '$_' found");
    }

    return $mail;
}

=head2 generate_preview

Generate a preview for a case. Similar to generate_email but without all the
required bits.

=cut

define_profile generate_preview => (
    optional => {
        body    => 'Str',
        subject => 'Str',
        to      => 'Str',
        from    => 'Str',
        cc      => 'Str',
        bcc     => 'Str',

        # These influence the 'TO'
        recipient_type => 'Str',
        recipient_role => 'Str',
        recipient      => 'Str',
    },
);

sub generate_preview {
    my $self = shift;
    my $opts = assert_profile({@_})->valid;

    my @keys = qw(body subject to from cc bcc);
    my %mail = map { $_ => $self->process_magic_strings($opts->{$_}) } @keys;

    if (defined $opts->{recipient_type}) {
        my $to = $self->process_recipient(%{$opts});
        $mail{to} = $to if defined $to;
    }

    return \%mail;
}

=head2 process_magic_strings

Replace a magic strings with actual values

    $self->process_magic_strings("Foo [[ bar ]]");

=cut

sub process_magic_strings {
    my ($self, $str) = @_;

    return '' unless defined $str;
    return $self->ztt->process_template($str)->string;
}

define_profile process_recipient => (
    required => { recipient_type => 'Str', },
    optional => {
        recipient_role => 'Str',
        recipient      => 'Str',
    },
);

=head2 process_recipient

Process a recipient based on the type role and/or uuid.

=cut

sub process_recipient {
    my $self = shift;
    my $opts = assert_profile({@_})->valid;

    my $recipient_type = $opts->{recipient_type};
    my $role           = lc($opts->{recipient_role} // '');
    my $recipient      = $opts->{recipient};
    my $case           = $self->case;


    if ($recipient_type eq 'overig') {
        my @list = Email::Address->parse($recipient);
        return unless @list;
        return join(",", map { $_->format } @list);
    }
    elsif ($recipient_type eq 'aanvrager') {
        return if $case->preset_client;
        return $case->aanvrager_object->email;
    }
    elsif ($recipient_type eq 'coordinator') {
        return unless $case->coordinator;
        return $case->coordinator_object->email;
    }
    elsif ($recipient_type eq 'behandelaar') {
        return unless $case->behandelaar;
        return $case->behandelaar_object->email;
    }
    elsif ($recipient_type eq 'gemachtigde') {
        return _list_to_email($case->pip_authorized_betrokkenen);
    }
    elsif ($recipient_type eq 'betrokkene') {
        return _list_to_email(
            $case->get_betrokkene_objecten({ 'LOWER(rol)' => $role })
        );
    }
    elsif ($recipient_type eq 'medewerker_uuid' && $self->has_betrokkene_model) {
        return $self->betrokkene_model->get(
            {
                intern  => 0,
                type    => 'medewerker',
            },
            $recipient,
        )->email;
    }
    return;
}

sub _list_to_email {
    return join(",", grep { defined $_ } map { $_->email } @_);
}

__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2019, Mintlab B.V. and all the persons listed in the
L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at
the L<LICENSE|Zaaksysteem::LICENSE> file.
