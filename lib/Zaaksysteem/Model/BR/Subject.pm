package Zaaksysteem::Model::BR::Subject;

use Moose;
use namespace::autoclean;

use BTTW::Tools;

use Catalyst::Model::Factory::PerRequest;
use Catalyst::Model::Factory;

extends 'Catalyst::Model::Factory::PerRequest';

=head1 NAME

Zaaksysteem::Model::BR::Subject - Catalyst glue for
L<Zaaksysteem::BR::Subject> instances.

=head1 DESCRIPTION

    my $brige = $c->model('BR::Subject');

=cut

__PACKAGE__->config(
    class       => 'Zaaksysteem::BR::Subject',
    constructor => 'new',
);

=head2 ACCEPT_CONTEXT

=cut

sub ACCEPT_CONTEXT {
    my ($self, $c, $args) = @_;

    $self->{ args } = $args;

    return Catalyst::Model::Factory::PerRequest::ACCEPT_CONTEXT($self, $c);
}

=head2 prepare_arguments

This method implements the interface required by
L<Catalyst::Model::Factory::PerRequest>.

=cut

sub prepare_arguments {
    my $self = shift;
    my $c = shift;

    my %args = (
        schema => $c->model('DB')->schema
    );

    if (ref $self->{ args } eq 'HASH') {
        %args = (%{ $self->{ args } }, %args);
    }

    return \%args;
}

__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2016, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=head1 UNDOCUMENTED FUNCTIONS

Below you will find a list of undocumented functions
Please find the time to fix them
This is done to start propper POD coverage testing on new modules

=head2 prepare_arguments

TODO: Fix the POD

=cut

