package Zaaksysteem::CLI::DashboardRemoval;
use Moose;

extends 'Zaaksysteem::CLI';
use BTTW::Tools;

around run => sub {
    my $orig = shift;
    my $self = shift;

    $self->assert_run;

    $self->do_transaction(
        sub {
            my ($self, $schema) = @_;

            my $object_model = $self->objectmodel;

            my $user;
            if (defined $self->options->{username}) {
                $user = $schema->resultset('Subject')->search_rs({username => $self->options->{username}});
            }
            elsif (defined $self->options->{id}) {
                $user = $schema->resultset('Subject')->search_rs({id => $self->options->{id}});
            }
            else {
                die("Unable to search for user, no username or ID provided");
            }

            while (my $s = $user->next) {
                my @widgets = $object_model->search_rs('widget',
                    { subject_id => $s->id })->all;

                foreach (@widgets) {
                    $self->log->info("Deleting widget with uuid " . $_->uuid);
                    $_->delete;
                }
            }
        }
    );
    return 1;

};

__PACKAGE__->meta->make_immutable;

__END__

=head1 NAME

Zaaksysteem::CLI::DashboardRemoval - Remove dashboards

=head1 DESCRIPTION

Removes dashboards for a user

=head1 COPYRIGHT and LICENSE

Copyright (c) 2019, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
