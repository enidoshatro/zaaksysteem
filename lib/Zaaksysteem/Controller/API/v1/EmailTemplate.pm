package Zaaksysteem::Controller::API::v1::EmailTemplate;
use Moose;
use namespace::autoclean;

BEGIN { extends 'Zaaksysteem::API::v1::Controller' }

=head1 NAME

Zaaksysteem::Controller::API::v1::EmailTemplate - API/v1 controller for email templates

=head1 DESCRIPTION

Search for and retrieve email templates.

=cut

use BTTW::Tools;
use Zaaksysteem::API::v1::ResultSet;

sub BUILD {
    my $self = shift;

    $self->add_api_context_permission('intern');
}

=head1 ACTIONS

=head2 base

Reserves the C</api/v1/email_template> routing namespace.

=cut

sub base : Chained('/api/v1/base') : PathPart('email_template') : CaptureArgs(0) { }

=head2 list

List or search for all email templates in Zaakssyteem.

Searching is done using ES query parameters:

=head3 URL Path

C</api/v1/email_template>

=head3 Parameters

=over

=item match:label=part_of_name

Retrieve email templates whose name matches the specified string case, case-insensitively.

=back

=cut

sub list : Chained('base') : PathPart('') : Args(0) {
    my ($self, $c) = @_;

    my $rs = $self->_get_objects($c);

    my $set = Zaaksysteem::API::v1::ResultSet->new(iterator => $rs);

    $c->stash->{set} = $set;

    $self->list_set($c);
}

sub _get_objects {
    my $self = shift;
    my $c = shift;

    my $rs = $c->model('DB::BibliotheekNotificaties')->search_rs(
        {
            deleted => undef,
        }
    );

    my $params = $c->req->params();
    delete $params->{page};

    my $query = {};

    if (keys %$params) {
        my $es = $c->parse_es_query_params->{query};

        if (defined $es->{match}{label}) {
            my $like = sprintf("%%%s%%", $es->{match}{label});

            $query->{label} = { ilike => $like };
        }
    }

    return $rs->search_rs($query);
}

__PACKAGE__->meta->make_immutable();

=head1 COPYRIGHT and LICENSE

Copyright (c) 2018, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
