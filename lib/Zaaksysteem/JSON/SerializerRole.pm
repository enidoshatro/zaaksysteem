package Zaaksysteem::JSON::SerializerRole;

use Moose::Role;

use BTTW::Tools;

use Zaaksysteem::Metarole::ObjectAttribute;

=head1 NAME

Zaaksysteem::JSON::SerializerRole - A role for objects to serialize to JSON
using L<Zaaksysteem::Metarole::ObjectAttribute>s.

=head1 DESCRIPTION

This role implements the logic of reading the L<metaclass|Class::MOP::Class>
of an object, introspecting the attributes defined with the C<Serialize>
trait, and building the hash to feed to an actual serializer implementation.

=head1 METHODS

=head2 serialize

This method implements the logic as described in L</DESCRIPTION>.

    Moose::Util::ensure_all_roles($object, 'Zaaksysteem::JSON::Serializer');

    my $content = $object->serialize;

=cut

sub serialize {
    my $self = shift;

    unless($self->can('meta')) {
        throw('json/serialize', 'Unable to introspect object\'s attributes, no ->meta sub');
    }

    my %properties;

    for my $attr ($self->meta->get_all_attributes) {
        next unless $attr->can('build_object_attribute_instance');

        my $reader = $attr->get_read_method;

        $properties{ $attr->name } = $self->$reader();
    }

    return \%properties;
}

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

