package Zaaksysteem::Event::EventInterface;
use Moose::Role;

use Carp qw(croak);
use BTTW::Tools;
use Zaaksysteem::API::v1::Message::Ack;

=head1 NAME

Zaaksysteem::Event::EventInterface - An interface for event handlers

=head1 DESCRIPTION

=head1 SYNOPSIS

    package Zaaksysteem::Event::XXXX;
    use Moose;
    with 'Zaaksysteem::Event::EventInterface';

    sub supports_event { 'myevent' }
    sub emit_event { ...; }

=cut

requires qw(
    event_type
    emit_event
);

has schema => (
    is       => 'ro',
    isa      => 'Zaaksysteem::Schema',
    required => 1,
);

has subject => (
    is       => 'ro',
    isa      => 'Zaaksysteem::BR::Subject',
    required => 1,
);

has queue => (
    is       => 'ro',
    isa      => 'Defined',
    required => 1,
);

has event => (
    is       => 'ro',
    isa      => 'HashRef', # should be an object, but yeah..
    required => 1,
);

has case => (
    is       => 'ro',
    isa      => 'Zaaksysteem::Model::DB::Zaak',
    required => 1,
);

sub supports_event {
    my ($self, $event) = @_;

    return 1 if $self->event_type eq $event;
    return 0;
}

sig get_template_by_name => 'Str';

sub get_template_by_name {
    my ($self, $name) = @_;

    my $id = $self->schema->resultset('Config')->get($name);

    if (!$id) {
        $self->log->info( "No template found for $name");
        return;
    }

    my $tpl = $self->schema->resultset('BibliotheekNotificaties')->find($id);
    return $tpl if $tpl;

    croak("Unable to find template with $name, $id not found in DB");
}

sub new_ack_message {
    my ($self, $msg) = @_;

    return Zaaksysteem::API::v1::Message::Ack->new(
        message => $msg,
    );
}

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2019, Mintlab B.V. and all the persons listed in the
L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at
the L<LICENSE|Zaaksysteem::LICENSE> file.

