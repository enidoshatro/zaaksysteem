package Zaaksysteem::Backend::Sysin::Modules::App::Word;
use Moose;
use namespace::autoclean;

extends 'Zaaksysteem::Backend::Sysin::Modules';

with qw/
    Zaaksysteem::Backend::Sysin::Modules::Roles::ProcessorParams
/;

=head1 NAME

Zaaksysteem::Backend::Sysin::Modules::APP::Word - Interface to configure a Word App in zaaksysteem

=head1 DESCRIPTION

This module allows the configuration of a Word App on zaaksysteem.nl

=cut

use BTTW::Tools;

use constant INTERFACE_ID => 'app_word';

use constant INTERFACE_CONFIG_FIELDS => [
    Zaaksysteem::ZAPI::Form::Field->new(
        name => 'interface_app_uri',
        type => 'display',
        label => 'XML URL',
        description => 'De link naar de XML noodzakelijk voor de installatie en gebruik van de Word app',
        data => {
            template => '<a href="<[field.value]>" target="_blank"><[field.value]></a>'
        }
    ),
];

use constant MODULE_SETTINGS => {
    name                          => INTERFACE_ID,
    module_type                   => [],
    label                         => 'App - MS Word',
    interface_config              => INTERFACE_CONFIG_FIELDS,
    direction                     => 'incoming',
    manual_type                   => ['text'],
    description                   => '<a target="_blank" href="http://wiki.zaaksysteem.nl/wiki/Redirect_app_word">Documentatie</a>',
    is_multiple                   => 0,
    is_manual                     => 0,
    retry_on_error                => 0,
    allow_multiple_configurations => 0,
    is_casetype_interface         => 0,
    has_attributes                => 0,
    trigger_definition => {},
    test_interface => 0,
    text_templates => {
        attributes => qq/
        Hier moet iets van tekst staan
        /,
    }
};

has configuration_key => (
    is      => 'ro',
    isa     => 'Str',
    default => 'app_enabled_wordapp',
);

around BUILDARGS => sub {
    my $orig  = shift;
    my $class = shift;

    return $class->$orig( %{ MODULE_SETTINGS() } );
};

=head2 api_full_access

Declares this module to have full access to the API within it's scope.

=cut

has api_full_access => (
    is      => 'ro',
    default => 1,
);

=head1 METHODS

=head2 _load_values_into_form_object

=cut

around _load_values_into_form_object => sub {
    my $orig = shift;
    my $self = shift;
    my $opts = $_[1]; # get options

    my $form = $self->$orig(@_);

    $form->load_values({
        interface_app_uri => $opts->{ base_url } . 'api/app/word/xml'
    });

    return $form;
};

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
