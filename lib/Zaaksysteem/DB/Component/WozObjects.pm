package Zaaksysteem::DB::Component::WozObjects;

use Moose;

use List::Util qw/min reduce/;
use JSON;

use BTTW::Tools;

extends 'DBIx::Class';

=head2 render

Add some additional formatting to the object, cleaner in Perl than in swig template

=cut

sub TO_JSON {
    my $self = shift;

    my $object_data = $self->object_data;

    my %derived = (
        bouwjaar => $self->bouwjaar,
        totale_oppervlakte => $self->totale_oppervlakte,
        totale_waarde => $self->totale_waarde,
        report_type => $self->report_type,
        monumentaanduiding => $self->monumentaanduiding,
        objectcodes => $self->objectcodes,
        partialobjectcodes => $self->partialobjectcodes
    );

    return {%derived, %$object_data};
}


=head2 bouwjaar

get the lowest non-zero bouwjaar from the partials

=cut

sub bouwjaar {
    my $self = shift;

    my $taxatie_onderdelen = $self->object_data->{onderbouwing_taxatie_onderdeel};

    # get the lowest non-zero bouwjaar from the partials
    return min grep { $_ > 0 } map { int($_->{Bouwjaar} || 0) } @$taxatie_onderdelen;
}


=head2 totale_waarde

add up the value of the partials

=cut

sub totale_waarde {
    my $self = shift;

    my $taxatie_onderdelen = $self->object_data->{onderbouwing_taxatie_onderdeel};

    # get the lowest non-zero bouwjaar from the partials
    return reduce { $a + $b } grep { $_ > 0 }
        map { int $_->{'Bepaalde waarde onderdeel'} } @$taxatie_onderdelen;
}

=head2 totale_oppervlakte

add up the square meters area for the partials

=cut

sub totale_oppervlakte {
    my $self = shift;

    my $taxatie_onderdelen = $self->object_data->{onderbouwing_taxatie_onderdeel};

    # get the lowest non-zero bouwjaar from the partials
    return reduce { $a + $b } grep { $_ > 0 } map { int $_->{Oppervlakte} } @$taxatie_onderdelen;
}

=head2 report_type

get the type of report, there are a few variations in the layout
related to this type. the following types are available right now:

- woning
- grond
- incourant
- agrarisch
- huurwaardekapitalisatie
- n.v.t.

=cut

sub report_type {
    my $self = shift;

    my $code = $self->object_data->{onderbouwing_taxatie}->{'Soort-object-code'}
        or throw ('woz_objects/get_report_type', 'Soort object code should be present here');

    my $model = $self->objectcodes->{$code}->{Model};

    my ($report_type) = $model =~ m|^model (.*)$|;
    return $report_type;
}

=head2 report_type_basic

Returns the "basic" report type -- either "woning" or "niet-woning".

=cut

sub report_type_basic {
    my $self = shift;

    my $report_type = $self->report_type;

    if ($report_type eq 'woning') {
        return 'woning';
    }
    else {
        return 'niet-woning';
    }
}


sub monumentaanduiding {
    my $self = shift;

    my $code = $self->object_data->{onderbouwing_taxatie}->{Monumentaanduiding};

    my $lookup = {
        # don't display this one, just be quiet. here for reference
        #0 => geen monument/onbekend
        1 => 'rijksmonument',
        2 => 'provinciaal monument',
        3 => 'gemeentelijk monument',
        4 => 'nominatie rijksmonument',
        5 => 'nominatie provinciaal monument',
        6 => 'nominatie gemeentelijk monument',
        7 => 'in onderzoeksfase om als monument te worden aangewezen',
        8 => 'beschermd stads- of dorpsgezicht',
    };

    return $code && $lookup->{$code} || '-';
}



has partialobjectcodes => (
    is => 'ro',
    lazy => 1,
    default => sub {
        shift->parse_csv("/share/stuf-tax/vianen/partialobjectcodes.csv", 'Ond.');
    }
);


=head2 objectcodes

Retrieve objectcodes lookup table.
Protect against the performance penalty of multiple invocations

=cut

has objectcodes => (
    is => 'ro',
    lazy => 1,
    default => sub {
        shift->parse_csv("/share/stuf-tax/vianen/objectcodes.csv", 'Soort');
    }
);


sub parse_csv {
    my ($self, $filename, $key) = @_;

    my $csv = Text::CSV->new({ binary => 1 })  # should set binary attribute.
        or throw("wozobjects/read_csv", "Cannot use CSV: " . Text::CSV->error_diag());

    my $homedir = $self->result_source->schema->catalyst_config->{home};

    # made it hardcoded for vianen now, if other config become applicable just
    # look in the config and insert customer id.
    open my $fh, "<:encoding(utf8)", $homedir . $filename
        or throw ("wozobjects/read_csv", "couldnt open objectcodes config file: $!");

    # header
    my $columns = $csv->getline($fh);
    my $objectcodes = {};

    while (my $row = $csv->getline( $fh )) {
        my %hash;
        @hash{@$columns} = @$row;

        throw('wozobjects/read_csv', 'Internal error, this should be truthy')
            unless exists $hash{$key};

        my $objectcode = $hash{$key};

        $objectcodes->{$objectcode} = \%hash;
    }

    $csv->eof or $csv->error_diag();
    close $fh;

    return $objectcodes;
}


1;



__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=cut


=head1 UNDOCUMENTED FUNCTIONS

Below you will find a list of undocumented functions
Please find the time to fix them
This is done to start propper POD coverage testing on new modules

=head2 TO_JSON

TODO: Fix the POD

=cut

=head2 monumentaanduiding

TODO: Fix the POD

=cut

=head2 parse_csv

TODO: Fix the POD

=cut

