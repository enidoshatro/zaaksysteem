package Zaaksysteem::DB::Component::Logging::Case::View;
use Moose::Role;

=head2 onderwerp

Creates a human readable subject for "case/view" type log entries.

=cut

sub onderwerp {
    my $self = shift;

    if (exists $self->data->{remote_system}) {
        return sprintf(
            "Opgevraagd door %s via %s.",
            $self->data->{remote_system} // '(onbekend)',
            $self->data->{interface} // '(onbekende bron)',
        );
    }
    else {
        return "Zaak opgevraagd.";
    }
}

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
