package Zaaksysteem::DB::Component::Logging::Subject::UpdateContactData;

use Moose::Role;
use namespace::autoclean;

=head1 NAME

Zaaksysteem::DB::Component::Logging::Subject::UpdateContactData - Log
formatter for C<subject/update_contact_data> events.

=head1 DESCRIPTION

This role covers logline formatting of C<subject/update_contact_data> events
for L<Zaaksysteem::DB::Component::Logging>.

=head1 METHODS

=head2 onderwerp

Implements logline formatting for L<Zaaksysteem::DB::Component::Logging>.

=cut

sub onderwerp {
    my $self = shift;

    my %slug_labels = (
        telefoonnummer => 'telefoonnummer',
        mobiel => 'telefoonnummer (mobiel)',
        note => 'interne notitie',
        email => 'e-mailadres'
    );

    my @labels = map { $slug_labels{ $_ } || $_ } @{ $self->data->{ fields } };

    return sprintf(
        'Plusgegevens voor betrokkene "%s" gewijzigd: %s',
        $self->subject->display_name,
        join (', ', @labels)
    );
}

sub _build_subject {
    my $self = shift;

    return $self->result_source->schema->betrokkene_model->get(
        {},
        $self->data->{ subject_id }
    );
}

sub _add_magic_attributes {
    shift->meta->add_attribute(subject => (
        is => 'ro',
        lazy => 1,
        builder => '_build_subject'
    ));
}

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2018, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
