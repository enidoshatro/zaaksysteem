package Zaaksysteem::Test::MailTrack;
use Moose;
extends 'Zaaksysteem::Test::Moose';

use autodie;
use utf8;

use Email::Address;
use Encode;
use File::Spec::Functions qw(catfile);
use IO::All;
use Mail::Track;
use Mail::Track::Message;
use Mail::Track::Message::Body;
use Zaaksysteem::Test;

sub _mail_track_ok {

    my $obj = Mail::Track->new(subject_prefix_name => 'Mintlab');

    isa_ok($obj, 'Mail::Track');
    can_ok(
        $obj, qw/
            subject_prefix_regex
            subject_prefix_name
            identifier_regex
            message

            parse

            prepare
            /
    );
    return $obj;
}

sub test_external_mail_track_message {

    my $obj = Mail::Track::Message->new();
    isa_ok($obj, "Mail::Track::Message");

    can_ok(
        $obj, qw/
            subject_prefix_regex
            message
            tmpdir

            entity
            identifier

            to
            from
            subject
            subject_raw

            attachments
            body

            add_body
            /
    );

    like($obj->tmpdir, qr#/tmp/\w+#, "Random tmp dir created");
}

sub test_external_mail_track_message_body {
    my $obj = new_ok('Mail::Track::Message::Body');

    can_ok(
        $obj, qw/
            as_text
            as_html

            parts
            entity
            /
    );
}

sub test_external_mail_track_subject {

    my $obj = _mail_track_ok();

    is($obj->subject_prefix_name, 'Mintlab',
        'Properly set prefix name on object');

    like($obj->identifier_regex, qr/\\d+/,
        'Default identifier_regex properly set');

    my $regex = $obj->subject_prefix_regex;
    $obj->identifier_regex(qr/jadda/);
    is($obj->identifier_regex, qr/jadda/, 'Can alter identifier_regex');

    isnt($regex, $obj->subject_prefix_regex,
        'Subject prefix changed after changing identifier_regex');

    $regex = $obj->subject_prefix_regex;
    $obj->subject_prefix_name('Foo');
    isnt($regex, $obj->subject_prefix_regex,
        'Subject prefix changed after changing subject_prefix_regex_name');

}

sub test_external_mail_track_subject_matching {

    my $obj   = _mail_track_ok();
    my $regex = $obj->subject_prefix_regex;

    like($obj->subject_prefix_regex,      qr/[Mintlab #\d+]/, 'Correct prefix');
    like("[Mintlab #42323-89923-89293A]", $regex,             'Matched regex');
    unlike("[Mintlab 42323-8924-98203A]", $regex, 'Skipped unreadable regex');

    like("Re: [Mintlab #42323-29023-8924A]",
        $regex, 'Match subject with Re: prefix');
    like("[Mintlab #42323-89283-89423A] This is a test",
        $regex, 'Match subject with subject content');
}

sub test_external_mail_track_message_handling {
    my $obj = _mail_track_ok();

    $obj->message( scalar io->catfile(qw(t data mails 101-plain_text_html_example.mail))->slurp );

    my $message = $obj->parse;

    is(
        $message->from,
        'Michiel Ootjers <michiel@example.com>',
        'Found From header'
    );
    is($message->to, 'zaaksysteem@example.com', 'Found To header');

    isnt($message->date, '');

    is($message->subject,     'Plain text MIME example', 'Found subject');
    is($message->subject_raw, 'Plain text MIME example', 'Found subject_raw');

    ok(!$message->identifier, 'No identifier set');

    isa_ok($message->body, 'Mail::Track::Message::Body',
        'Found message body object');
    like($message->body->as_text, qr/This is a plain text mail/, 'Found body');
    like($message->body->as_html, qr/This is a plain text mail/, 'Found body');

    like(
        $message->body->as_text,
        qr/Some strange characters like Gar\xc3\xa7on/,
        'Proper ISO to UTF-8 parsing'
    );

    like(
        $message->body->as_text,
        qr/Confusing quoted-printable: "=00"/,
        'Proper quoted-printable decoding'
    );
}

sub test_external_mail_track_attachments {
    my $obj = _mail_track_ok();

    $obj->message( scalar io->catfile(qw(t data mails 102-attachment_mail_example.mail))->slurp );

    my $message = $obj->parse;

    can_ok(
        $message->attachments->[0], qw/
            filename
            filename_without_extension
            extension
            path

            entity
            /
    );

    is(scalar @{ $message->attachments }, 2, 'Found 2 attachments');

    for my $att (@{ $message->attachments }) {
        ok($att->$_,
            'Found filled attribute for file "' . $att->filename . '": ' . $_)
            for qw/extension filename_without_extension filename path/;
    }
}

sub external_mail_track_subject_parsing {
    my $obj = Mail::Track->new(subject_prefix_name => 'Mintlab',);

    $obj->message( scalar io->catfile(qw(t data mails 105-plain_text_identifier.mail))->slurp );

    my $message = $obj->parse;

    is(
        $message->subject_raw,
        '[Mintlab #974923-8844-2392N] Plain text MIME example',
        'Found proper subject: ' . $message->subject_raw
    );
    is(
        $message->subject,
        'Plain text MIME example',
        'Found extracted subject: ' . $message->subject
    );
    is($message->identifier, '974923-8844-2392N',
        'Identifier set to: ' . $message->identifier);

    like($message->body->as_text, qr/This is a plain text mail/, 'Found body');
}

sub test_external_mail_track_sending {
    my $params = {
        to      => 'michiel@mintlab.nl',
        from    => 'michiel@ootjers.nl',
        subject => 'Foobar',
    };

    my $obj = Mail::Track->new(
        subject_prefix_name  => 'Testsuite',
        identifier_regex     => qr/(\d{4})/,
        identifier           => '1234',
    );

    my $msg = $obj->prepare({ %$params, Body => 'This is some content', });

    is($msg->$_, $params->{$_}, 'Correct param $message->' . $_)
        for keys %$params;

    ok(
        $msg->add_body(
            {
                content      => "Some strange characters like Gar\xc3\xa7on",
                content_type => 'text/plain',
            }
        ),
        'Added a text body'
    );
    is(
        $msg->body->as_text,
        "Some strange characters like Gar\xc3\xa7on",
        "Text body is correct"
    );

    ok(
        $msg->add_body(
            {
                content      => 'This is some html <b>bold</b> yeah',
                content_type => 'text/html',
            }
        ),
        'Added a html body'
    );
    is(
        $msg->body->as_html,
        'This is some html <b>bold</b> yeah',
        "HTML is correct"
    );

    $msg->add_attachment( path => catfile(qw(t data documents openoffice_document.pdf)) );
    $msg->add_attachment(
        path     => catfile(qw(t data documents openoffice_document.pdf)),
        filename => 'Optional, deduct from path',
        mimetype => 'application/txt',
    );

    my @attachments = @{ $msg->attachments };
    is(@attachments, 2, "Two attachments found");

    is($attachments[0]->mimetype,
        'application/pdf', "Guessing mimetype is correct");
    is($attachments[0]->filename,
        'openoffice_document.pdf', "Deduction filename is correct");

    is($attachments[1]->mimetype, 'application/txt', "MIME type overwrite");
    is(
        $attachments[1]->filename,
        'Optional, deduct from path',
        "Filename overwrite"
    );

    my $entity = $msg->_build_entity;
    my $size = $msg->size;

    is($size, length($entity->as_string), "Message has the correct size");

    my $message;
    my $opts;
    {
        no warnings qw(redefine once);
        local *Email::Sender::Simple::send = sub {
            my $self = shift;
            my $entity = shift;
            $opts  = shift;
            $message = $entity->stringify();
        };
        $msg->send;
    }
    isa_ok(delete $opts->{transport}, "Email::Sender::Transport::SMTP");
    is_deeply($opts, {}, "No options given");
    ok($message, 'Sending e-mail');

    # We should be able to parse our own message right?
    my $own = $obj->parse($message);

    is($own->subject_raw, '[Testsuite 1234] Foobar', 'Found proper subject');
    is($own->subject,    'Foobar', 'Found extracted subject, Foobar');
    is($own->identifier, '1234',   'Found identifier 1234');

    is(
        $own->body->as_text,
        "Some strange characters like Gar\xc3\xa7on",
        'Text body is found'
    );
    is(
        $own->body->as_html,
        "This is some html <b>bold</b> yeah",
        'HTML body is found'
    );
    is(@{ $own->attachments }, 2, "One attachments found");

    my ($a, $b) = @{ $own->attachments };

    like($a->path, qr#/tmp/\w+/msg-[\w\-]+/openoffice_document.pdf#,
        'Path found');
    is($a->filename, 'openoffice_document.pdf', 'Filename found');
    is($a->mimetype, 'application/pdf',         "MIME type is correct");

    like($b->path, qr#/tmp/\w+/msg-[\w\-]+/Optional, deduct from path#,"Correct filename");
    is($b->filename, 'Optional, deduct from path', 'Filename found');
    is($b->mimetype, 'application/txt', "MIME type is correct");
}

sub test_external_mail_track_sending_recipients {
    my $params = {
        from    => 'bar@example.com',
        to      => 'foo@example.net,bis@example.net',
        cc      => 'baz@example.org,dmz@example.org',
        bcc     => 'fubar@example.com , fyi@example.com',
        subject => "Hide 'n seek",
    };

    my $obj = _mail_track_ok;

    my $msg = $obj->prepare({ %$params, Body => 'This is some content', });
    my $message;
    my $opts = [];

    no warnings qw(redefine once);
    local *Email::Sender::Simple::send = sub {
        my $self = shift;
        my $entity = shift;
        push(@$opts, shift);
        $message = $entity->stringify();
    };
    $msg->send;
    ok($message, 'Message send!');

    is(@$opts, 3, 'Sending three mails: to/cc, 2 bcc');
    my $to_cc = shift(@$opts);
    isa_ok(delete $to_cc->{transport}, "Email::Sender::Transport::SMTP");

    my $bcc = shift(@$opts);
    my ($f, $s) = Email::Address->parse($params->{bcc});
    $f = $f->address;
    $s = $s->address;

    isa_ok(delete $bcc->{transport}, "Email::Sender::Transport::SMTP");
    is_deeply($bcc, { to => $f }, "BCC'ed to: $f");

    $bcc = shift(@$opts);
    isa_ok(delete $bcc->{transport}, "Email::Sender::Transport::SMTP");
    is_deeply($bcc, { to => $s }, "BCC'ed to: $s");

}

sub test_external_mail_track_parsing_simple {
    my $obj = _mail_track_ok();
    my $msg = $obj->parse( scalar io->catfile(qw(t data mails marco.mime))->slurp );

    is($msg->subject_raw, 'Banana', 'Found proper subject');
    is($msg->subject,     'Banana', 'Found extracted subject');
    is($msg->identifier,  undef,    'No identifier is found in the subject');

    is($msg->body->as_text,    "\n", 'No body is found');
    is(@{ $msg->attachments }, 3,    "Three attachments found");
}

sub test_external_mail_track_utf8_headers {
    my $obj = Mail::Track->new();
    my $msg = $obj->parse( scalar io->catfile(qw(t data mails utf_in_headers.mime))->slurp );

    is($msg->get_from_entity_head('subject'), "t\N{U+00E9}st", "Subject parsed correctly");
    is($msg->get_from_entity_head('to'), "Pïét ädmin <email\@sprint.zaaksysteem.nl>", "to parsed correctly");

    is($msg->subject_raw, "t\N{U+00E9}st", "Subject parsed correctly");
    is($msg->to, "Pïét ädmin <email\@sprint.zaaksysteem.nl>", "to parsed correctly");
}

sub test_external_mail_track_parsing_odd_chars {
    my $obj = _mail_track_ok;
    my $msg = $obj->parse( scalar io->catfile(qw(t data mails mail_with_odd_chars.mime))->slurp );

    is(
        $msg->from,
        "W\N{U+00EB}sley Schwengle <wesley\@mintlab.nl>",
        "Found proper from with encoding"
    );

    is($msg->subject_raw, '',    'Found proper subject');
    is($msg->subject,     '',    'Found extracted subject');
    is($msg->identifier,  undef, 'No identifier is found in the subject');

    is(
        $msg->body->as_text, '-- ' . q{
Wesley Schwengle, Backend Developer
Mintlab B.V., http://www.mintlab.nl / http://www.zaaksysteem.nl
E: wesley@mintlab.nl
T:  +31 20 737 00 05
}
        , 'Correct body is found'
    );
    is(@{ $msg->attachments }, 1, "One attachments found");
}

sub test_external_mail_track_outlook {
    my $obj = _mail_track_ok();
    my $msg = $obj->parse( scalar io->catfile(qw(t data mails outlook.mime))->slurp );

    is($msg->subject_raw, 'Topmail: Headertext', 'Found proper subject');
    is($msg->subject, 'Topmail: Headertext', 'Found extracted subject');
    is($msg->identifier, undef, 'No identifier is found in the subject');

    is($msg->body->as_text, "Topmail: Bodytext\n\n", 'Body is found');
    is(@{ $msg->attachments }, 2, "Two attachments found");

    my @nested = @{ $msg->nested_messages };
    is(@nested, 1, "One nested message");

    {
        my $msg = $nested[0];
        isa_ok($msg, "Mail::Track::Message");
        is($msg->subject_raw, 'Submail: Headertext', 'Found proper subject');
        is($msg->subject, 'Submail: Headertext', 'Found extracted subject');
        is($msg->identifier, undef, 'No identifier is found in the subject');

        is($msg->body->as_text, "Submail: Bodytext\n\n", 'Body is found');
        is(@{ $msg->attachments }, 2, "Two attachments found");
    }

}

sub test_external_mail_track_subject_prefix {
    my $obj = Mail::Track->new(subject_prefix_name => 'Mintlab');
    my $msg = $obj->parse(<<'EOM'
Received: by 10.52.240.136 with HTTP; Wed, 4 Feb 2015 05:40:20 -0800 (PST)
Date: Wed, 4 Feb 2015 14:40:20 +0100
Subject: Prefix [Mintlab #1234-5678-1234X] Suffix
From: Martijn van de Streek <martijn@mintlab.nl>
To Martijn van de Streek <martijn@mintlab.nl>
Content-Type: multipart/alternative; boundary=089e01635330260291050e435473

--089e01635330260291050e435473
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: base64

dMOpw6nDqcOpw6lzdA0K
--089e01635330260291050e435473--
EOM
    );

    is($msg->subject_raw, 'Prefix [Mintlab #1234-5678-1234X] Suffix', 'Found proper subject');
    is($msg->subject, 'Prefix Suffix', 'Found extracted subject');
    is($msg->identifier, '1234-5678-1234X', 'Found identifier');
}

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2014, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
